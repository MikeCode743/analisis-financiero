/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import $ from 'jquery';
window.$ = window.jQuery = $;
window.Vue = require('vue');
import Vuetify from "../plugins/vuetify";
import 'vuetify/dist/vuetify.min.css';
import Chartkick from 'vue-chartkick';
import Chart from 'chart.js';
Vue.use(Chartkick.use(Chart));

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.component('estructura-balance', require('./components/EstructuraBalance').default);
Vue.component('agregar-balance', require('./components/AgregarBalance').default);

Vue.component('estructura-estado', require('./components/EstructuraEstado').default);
Vue.component('agregar-estado', require('./components/AgregarEstado').default);

Vue.component('dato-empresa', require('./components/DatoEmpresa').default);

//Razones Financieras
Vue.component('razones', require('./components/Razones').default);

//Ratio Grupo
Vue.component('ratio-grupo', require('./components/AgregarRatioGrupo').default);

//Mensaje Razon
Vue.component('mensaje-razon', require('./components/MensajeRazon').default);

//Comparacoin por Sector
Vue.component('razon-sector', require('./components/RazonSector').default);

//Panel de navegacion
Vue.component('panel-navegacion', require('./components/PanelNavegacion').default);

//OCR para obtener tabla excel
Vue.component('tabla-ocr', require('./components/TableOCR').default);

//CRUD de ratios
Vue.component('ratio-cru', require('./components/RatioCRU').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    vuetify : Vuetify,
    el: '#app',
});
