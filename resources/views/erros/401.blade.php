@extends('layouts.app')


@section('content')

<body>
    <!-- Custom stlylesheet -->
    <link rel="stylesheet" href="{{ asset('css/styleerrors.css') }}" />
    <link href="https://fonts.googleapis.com/css?family=Maven+Pro:400,900" rel="stylesheet">

    <div id="notfound">
        <div class="notfound">
            <h2>Lo sentimos, No estas autorizado para ingresar a esta funcion</h2>
            <p>Esta pagina realiza funciones que solo el administrador y usuarios con mayor rango pueden acceder.</p>
            <a href="{{ route('home') }}">Volver al inicio</a>
        </div>
    </div>

</body>

</html>
@endsection