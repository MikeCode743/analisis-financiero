@extends('layouts.app')
<title>Ratios</title>
@section('librerias')
<script src="{{ asset('js/app.js') }}" defer></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection
@section('content')
<div id="app">
    <ratio-cru></ratio-cru>
</div>
@endsection()