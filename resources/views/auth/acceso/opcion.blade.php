@extends('layouts.app')

@section('librerias')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>
@endsection

{{-- @if (Auth::user()->acceso('000')) --}}
    
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10 mx-auto-3 my-3">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-9">
                            <h5 class="card-title  float-left">Accesos registrados</h5>
                        </div>
                        <div class="col-sm-3">
                            @if (Auth::user()->acceso('000'))
                                
                            <a href="#add_Modal" class="btn btn-success" data-toggle="modal">
                                <span>Agregar Acceso</span></a>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-title text-center">Accesos Disponibles</h5>
                    @if($accesos)
                    <table id="acceso" name="acceso" class="table display table-sm table-hover">
                        <thead>
                            <tr>
                                <th scope="col" width="20%">Identificador</th>
                                <th scope="col" width="50%">Descripcion</th>
                                @if (Auth::user()->acceso('000'))
                                <th scope="col" class="text-center">Accion</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($accesos as $acceso)
                            <tr>
                                <td>{{ $acceso->id_opcion}}</td>
                                <td>{{ $acceso->descripcion_opcion}}</td>
                                @if (Auth::user()->acceso('000'))
                                    
                                <td class="text-center">
                                    <button type="button" class="btn btn-outline-info btn-edit btn-sm"
                                    data-toggle="modal" data-target="#editModal">Editar</button>
                                    
                                    <button type="button" class="btn btn-outline-danger btn-delete btn-sm" data-toggle="modal" data-target="#deleteModal">Eliminar</button>
                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @else
            <h2>No hay Accesos</h2>
            @endif
        </div>
    </div>
</div>

<!-- ADD Modal HTML -->
<div id="add_Modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{route('acceso.store')}}" method="post" id="form_add">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">Agregar Acceso</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        {{-- Descripcion --}}
                        <label>Descripcion</label>
                        <textarea minlength="6" class="form-control {{$errors->has('descripcion') ? 'is-invalid' : ''}}"
                            id="descripcion" name="descripcion" rows="3" required=""
                            oninvalid="this.setCustomValidity('Este campo es requerido')"
                            oninput="setCustomValidity('')"></textarea>
                        @if($errors->has('descripcion'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('descripcion') }}</strong>
                        </span>
                        @endif

                        <label class="mt-3">Sector</label>
                        <select name="rol" class="form-control {{$errors->has('rol') ? 'is-invalid' : ''}}" id="rol">
                            <option value="">Seleccionar Rol</option>
                            @if ($roles)
                            @foreach ($roles as $rol)
                            <option value="{{ $rol }}"
                                {{(old('rol') == $rol) ? 'selected="true"' : ''}}>
                                {{ $rol }}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Agregar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- EDIT Modal HTML -->
<div id="editModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" id="editForm">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">Agregar Acceso</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        {{-- Descripcion --}}
                        <label>Descripcion</label>
                        <textarea minlength="6" class="form-control {{$errors->has('descripcion') ? 'is-invalid' : ''}}"
                            id="editDescripcion" name="editDescripcion" rows="3" required=""
                            oninvalid="this.setCustomValidity('Este campo es requerido')"
                            oninput="setCustomValidity('')"></textarea>
                        @if($errors->has('descripcion'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('descripcion') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Agregar</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- Eliminar Modal --}}
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" action="" id="deleteForm">
                @csrf
                @method('DELETE')
                <div class="modal-header">
                    <h5 class="modal-title" id="">Eliminar Acceso</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                        ¿Está seguro de querer eliminar el acceso seleccionado?
                        <p style="color: red">
                            Los cambios podrian ser permanentes y podria traer problemas al sistema
                        </p>
                    </p>
                    <input type="hidden" name="_method" value="DELETE">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" type="submit">Sí, seguro</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready( function () {
        var table = $('#acceso').DataTable();

        table.on('click','.btn-edit',function(){
            $('#editForm').get(0).reset();
        $tr = $(this).closest('tr');
            $('#editModal').data('row', $tr);
        if($($tr).hasClass('child')){
            $tr=$tr.prev('.parent');
        }
            $('#editModal').modal('show');
        });

        $('#editModal').on('shown.bs.modal', function (e){
        var data = table.row($(this).data('row')).data();
        $('#editDescripcion').val(data[1]).focus();
        $('#editForm').attr('action', window.location.pathname+'/'+data[0]);
        $('#editForm').attr('method', 'post');
        });

        table.on('click','.btn-delete',function(){
        $tr = $(this).closest('tr');
        $('#deleteModal').data('row', $tr);
        if($($tr).hasClass('child')){
        $tr=$tr.prev('.parent');
        }
        var data = table.row($tr).data();
        $('#deleteForm').attr('action', window.location.pathname+'/'+data[0]);
        $('#deleteForm').attr('method', 'post');
        $('#deleteModal').modal('show');
        });

    });
</script>

@endsection