@extends('layouts.app')

@section('librerias')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            @if($opcionesusuario->isNotEmpty())
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-start d-inline-flex">
                        Lista de Accesos
                    </div>
                    

                </div>
                <div class="card-body">
                    <table id="acceso" name="acceso" class="table display table-sm table-hover">
                        <thead>
                            <tr>
                                <th scope="col" width="20%">Identificador</th>
                                <th scope="col" width="50%">Descripcion</th>
                                <th scope="col" class="text-center">Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($opcionesusuario as $opc)
                            <tr>
                                <td>{{ $opc->id_opcion}}</td>
                                <td>{{ $opc->descripcion }}</td>
                                <td class="text-center">
                                    <button type="button" class="btn btn-outline-danger btn-delete btn-sm"
                                        data-toggle="modal" data-target="#modal-delete">Eliminar</button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
                <div class="card-footer text-muted">
                    <div class="d-flex justify-content-end ">
                        <button type="button" class="btn btn-danger" data-toggle="modal"
                            data-target="#deteleEmpleadoModal">
                            Eliminar Empleado
                        </button>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
<br>
<br>
<br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            @if($opciones->isNotEmpty())
            <div class="card">
                <div class="card-header">
                    Accesos Disponibles
                </div>
                <div class="card-body">
                    <table id="disponible" name="disponible" class="table display table-sm table-hover">
                        <thead>
                            <tr>
                                <th scope="col" width="20%">Identificador</th>
                                <th scope="col" width="50%">Descripcion</th>
                                <th scope="col" class="text-center">Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($opciones as $opc)
                            <tr>
                                <td>{{ $opc->id_opcion}}</td>
                                <td>{{ $opc->descripcion}}</td>
                                <td class="text-center">
                                    <button type="button" class="btn btn-outline-success btn-add btn-sm"
                                        data-toggle="modal" data-target="#modal-add">Agregar</button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
            @endif
        </div>
    </div>
</div>



{{-- Eliminar Modal --}}
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" action="" id="form-delete">
                @csrf
                @method('DELETE')
                <div class="modal-header">
                    <h5 class="modal-title" id="">Eliminar Acceso</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                        ¿Está seguro de querer eliminar el acceso seleccionado?
                    </p>
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" id="opcU" name="opcU" value="">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" type="submit">Sí, seguro</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- Add Modal --}}
<div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" action="" id="form-add">
                @csrf
                @method('POST')
                <div class="modal-header">
                    <h5 class="modal-title" id="">Agregar acceso</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                        ¿Desar agregar la nueva opcion de acceso?
                    </p>
                    <input type="hidden" id="opc" name="opc" value="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-success" type="submit">Sí, seguro</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- DeleteEmpleadoModal -->
<div class="modal fade" id="deteleEmpleadoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="">Eliminar Acceso</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('empresa.empleado.destroy',[$empresa->id_empresa,$empleado->id]) }}" id="form-delete">
                    @csrf
                    @method('DELETE')
                    
                    <div class="modal-body">
                        <p>
                            ¿Está seguro de querer eliminar el acceso seleccionado?
                        </p>
                        <input type="hidden" name="_method" value="DELETE">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Eliminar Empleado</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready( function () {

        var table = $('#acceso').DataTable({
            "searching": false,
            });
        
        table.on('click','.btn-delete',function(){
            
        $tr = $(this).closest('tr');
        $('#modal-delete').data('row', $tr);
        if($($tr).hasClass('child')){
        $tr=$tr.prev('.parent');
        }
        
        var data = table.row($tr).data();
        $('#opcU').val(data[0]);
        $('#modal-delete').modal('show');
        });


        var tdisponible = $('#disponible').DataTable({
        "searching": false
        });
        tdisponible.on('click','.btn-add',function(){
            
        $tr = $(this).closest('tr');
        $('#modal-add').data('row', $tr);
        if($($tr).hasClass('child')){
        $tr=$tr.prev('.parent');
        }
        var data = tdisponible.row($tr).data();
        $('#opc').val(data[0]);
        $('#modal-add').modal('show');
        });

    } );
</script>

@endsection