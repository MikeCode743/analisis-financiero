@extends('layouts.app')
@section('librerias')
<script src="{{ asset('js/app.js') }}" defer></script>
@endsection
@section('content')
<div id="app">
    <dato-empresa empresa={{$id}} nombre="{{$empresa->nombre}}"></dato-empresa>
</div>
@endsection