@extends('layouts.app')

@section('librerias')
<link rel="stylesheet"
    href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">

@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="list-group">
                <a href="#" class="list-group-item list-group-item-action active">
                    <i class="las la-building" style="font-size:16px"></i> {{ $empresa->nombre }}
                </a>
                @if(Auth::user()->acceso('010'))
                <a href="{{ url('/balancegeneral/'.$empresa->id_empresa.'/configurar') }}"
                    class="list-group-item list-group-item-action"><i class="las la-stream" style="font-size:16px"></i>
                    {{ __('Balance General (Configurar)') }}</a>
                    @endif
                @if(Auth::user()->acceso('020'))
                <a href="{{ route('balance.agregar',$empresa->id_empresa) }}"
                    class="list-group-item list-group-item-action"><i class="las la-chart-area"
                        style="font-size:16px"></i>{{ __('Balance General (Agregar)') }}</a>
                @endif
                @if(Auth::user()->acceso('011'))
                <a href="{{ route('estado.inicio',$empresa->id_empresa) }}"
                    class="list-group-item list-group-item-action"><i class="las la-stream"
                        style="font-size:16px"></i>{{ __('Estado Resultado (Configurar)') }}</a>
                @endif
                @if(Auth::user()->acceso('011'))
                <a href="{{ route('estado.agregar',$empresa->id_empresa) }}"
                    class="list-group-item list-group-item-action"><i class="las la-wallet"
                        style="font-size:16px"></i>{{ __('Estado Resultado (Agregar)') }}</a>
                @endif
                @if(Auth::user()->acceso('011'))
                <a href="{{ route('ratio.detalle.configurar',$empresa->id_empresa) }}"
                    class="list-group-item list-group-item-action"><i class="las la-exchange-alt"></i>Configurar Cuentas
                    para An&aacute;lisis de Ratios</a>
                @endif
                @if(Auth::user()->acceso('012'))
                <a href="{{ route('estadosfinancieros.lista',$empresa->id_empresa) }}"
                    class="list-group-item list-group-item-action"><i class="las la-archive"
                        style="font-size:16px"></i>{{ __('Estados Financieros por Periodo') }}</a>
                @endif
                @if(Auth::user()->acceso('018'))
                <a href="{{ route('ratio.empresa',$empresa->id_empresa) }}"
                    class="list-group-item list-group-item-action"><i class="las la-percent"
                        style="font-size:16px"></i>An&aacute;lisis de Ratios</a>
                @endif
                @if(Auth::user()->acceso('018'))
                <a href="{{ url('empresa/'.$empresa->id_empresa) }}" class="list-group-item list-group-item-action"><i
                        class="las la-balance-scale"></i>{{ __('Estados Financieros Detallados') }}</a>
                <a class="list-group-item list-group-item-action" href="{{ route('ratio.sector',$empresa->id_sector) }}" role="button"><i class="las la-industry" style="font-size:16px"></i>Comparaci&oacute;n de Ratios del Sector
                        </a>
                @endif
            </div>
            @if(Auth::user()->acceso('018'))
            <div class="card-footer text-muted">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#NuevoEmpleadoModal">
                    Registrar Nuevo Empleado
                </button>
            </div>
            @endif
        </div>
    </div>
</div>
<br>
<br>
<br>
@if(Auth::user()->acceso('018'))
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            @if($empleados->isNotEmpty())
            <div class="card">
                <div class="card-header">
                    Lista de Empleados
                </div>
                <div class="card-body">
                    <table class="table table-sm table-hover">
                        <thead>
                            <tr>
                                <th scope="col" width="20%">Nombre</th>
                                <th scope="col" width="20%">Email</th>
                                <th scope="col" class="text-center">Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($empleados as $empleado)
                            <tr>
                                <td>{{ $empleado->nombre }}</td>
                                <td>{{ $empleado->email}}</td>
                                <td class="text-center">
                                    <a class="btn btn-primary btn-sm"
                                        href="{{ route('empresa.empleado.detalle',[$empresa->id_empresa,$empleado->id]) }}"
                                        role="button">Ver
                                        Acceso</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endif
<!-- ADD Modal HTML -->
<div id="NuevoEmpleadoModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Agregar Empleado</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('empresa.empleado',$empresa->id_empresa) }}">
                    @csrf
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email"
                            class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                name="email" value="{{ old('email') }}" required autocomplete="email">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                        <div class="col-md-6">
                            <input id="password" type="password"
                                class="form-control @error('password') is-invalid @enderror" name="password" required
                                autocomplete="new-password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password-confirm"
                            class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control"
                                name="password_confirmation" required autocomplete="new-password">
                        </div>
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary btn-lg btn-block">
                                {{ __('Register') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



@endsection