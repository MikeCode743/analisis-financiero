@extends('layouts.app')
@section('librerias')
<script src="{{ asset('js/app.js') }}" defer></script>
@endsection
@section('content')
<div id="app">
    <razones empresa={{$id}} nombre="{{$empresa->nombre}}"></razones>
</div>
@endsection