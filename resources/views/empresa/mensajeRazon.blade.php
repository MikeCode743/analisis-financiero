@extends('layouts.app')
@section('librerias')
<script src="{{ asset('js/app.js') }}" defer></script>
@endsection
@section('content')
<div id="app">
    <mensaje-razon empresa={{$id}} nombre="{{$empresa->nombre}}" itemempresa="{{$listaAnio}}" datorazon="{{$listaRazon}}"></mensaje-razon>
</div>
@endsection