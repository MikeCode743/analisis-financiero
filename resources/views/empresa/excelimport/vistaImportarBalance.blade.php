

<div class="container">

  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">catalogo balance</div>
        <div class="panel-body">
          <form method="post" action="{{route('importar.balance.catalogo', [2])}}" accept-charset="UTF-8" enctype="multipart/form-data">
          
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
          
            <div class="form-group">
              <label class="col-md-4 control-label">Nuevo Archivo</label>
              <div class="col-md-6">
                <input type="file" class="form-control" name="catalogoexcel" >
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">Enviar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">datos balance</div>
        <div class="panel-body">
          <form method="post" action="{{route('importar.balance.valores', [2])}}" accept-charset="UTF-8" enctype="multipart/form-data">
          
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
          
            <div class="form-group">
              <label class="col-md-4 control-label">Nuevo Archivo</label>
              <div class="col-md-6">
                <input type="file" class="form-control" name="catalogoexcel" >
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">Enviar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">catalogo resultado</div>
        <div class="panel-body">
          <form method="post" action="{{route('importar.resultados.catalogo', [2])}}" accept-charset="UTF-8" enctype="multipart/form-data">
          
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
          
            <div class="form-group">
              <label class="col-md-4 control-label">Nuevo Archivo</label>
              <div class="col-md-6">
                <input type="file" class="form-control" name="catalogoexcel" >
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">Enviar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">datos resultado</div>
        <div class="panel-body">
          <form method="post" action="{{route('importar.resultados.valores', [2])}}" accept-charset="UTF-8" enctype="multipart/form-data">
          
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
          
            <div class="form-group">
              <label class="col-md-4 control-label">Nuevo Archivo</label>
              <div class="col-md-6">
                <input type="file" class="form-control" name="catalogoexcel" >
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">Enviar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

{{----}}
<div>
  @if(isset($catalogoImportado))
    @foreach($catalogoImportado as $c)
      <table class="table">
        <tr>
          <td>{{$c[0]}}</td>
          <td>{{$c[1]}}</td>
        </tr>
      </table>
    @endforeach
  @endif
</div>

{{----}}
<div>
  @if(isset($valoresImportados))
    @foreach($valoresImportados as $val)
      <table class="table">
        <tr>
          <td>{{$val[0]}}</td>
          <td>{{$val[1]}}</td>
        </tr>
      </table>
    @endforeach
  @endif
</div>

{{----}}
<div>
  @if(isset($catalogoImportado2))
    @foreach($catalogoImportado2 as $c)
      <table class="table">
        <tr>
          <td>{{$c[0]}}</td>
          <td>{{$c[1]}}</td>
        </tr>
      </table>
    @endforeach
  @endif
</div>

{{----}}
<div>
  @if(isset($valoresImportados2))
    @foreach($valoresImportados2 as $val)
      <table class="table">
        <tr>
          <td>{{$val[0]}}</td>
          <td>{{$val[1]}}</td>
        </tr>
      </table>
    @endforeach
  @endif
</div>