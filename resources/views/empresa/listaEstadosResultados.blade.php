@extends('layouts.app')

@section('librerias')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.15.3/dist/sweetalert2.all.min.js"></script>

@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if($estadosesultados->isNotEmpty())
            <div class="card">
                <div class="card-header">
                    Empresa : {{ $empresa->nombre }}
                </div>
                <div class="card-body">
                    <table id="tablabalance" class="table table-sm table-hover">
                        <thead>
                            <tr>
                                <th scope="col" width="20%">Año</th>
                                <th scope="col" width="20%">Balance</th>
                                <th scope="col" class="text-center">Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($estadosesultados as $estado)
                            <tr>
                                <td>{{ $estado->anio }}</td>
                                <td><a>Estado Resultado</a></td>
                                <td class="text-center">
                                    <button class="btn btn-danger" id="btneliminar" name="btneliminar" onclick="estadoresultadodelete({{ $estado->anio }}, {{ $empresa->id_empresa }})">Eliminar</button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">

                </div>
            </div>
            @endif
        </div>
    </div>
</div>


<script src="https://unpkg.com/axios/dist/axios.min.js">
    const axios = require('axios');
</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10">
    const Swal = require('sweetalert2');
</script>

<script type="text/javascript">
    function estadoresultadodelete(anio, id) {

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(function(isConfirm) {
            if (isConfirm.value === true) {
                axios.post('{{ route('resultado.eliminar')}}', {
                            empresa: id,
                            anio: anio
                        }).then(response => {
                        console.log(response.data.message);
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 1500,
                            timerProgressBar: true,
                            didOpen: (toast) => {
                                toast.addEventListener('mouseenter', Swal.stopTimer)
                                toast.addEventListener('mouseleave', Swal.resumeTimer)
                            }
                        })
                        Toast.fire({
                            icon: 'success',
                            title: response.data.message
                        }).then(function() {
                            location.reload(true);
                        });
                    })
                    .catch(error => {
                        console.log(error)
                        Swal.fire({
                            position: 'top-end',
                            icon: 'error',
                            title: response.data.message,
                            showConfirmButton: false,
                            timer: 2000
                        })

                    });

            } //delete item
        }, function(dismiss) {
            if (dismiss === 'cancel' || dismiss === 'close') {
                // ignore
            }
        })

    }
</script>

@endsection