@extends('layouts.app')
@section('librerias')
<script src="{{ asset('js/app.js') }}" defer></script>
@endsection
@section('content')
<div id="app">
    <razon-sector dato="{{$dato}}" comparar="{{$comparar}}"></razon-sector>
</div>
@endsection