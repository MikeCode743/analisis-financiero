@extends('layouts.app')
@section('librerias')
<script src="{{ asset('js/app.js') }}" defer></script>
@endsection
@section('content')
<div id="app">
    <estructura-estado empresa={{$id}} nombre="{{$empresa->nombre}}"></estructura-estado>
</div>
@endsection