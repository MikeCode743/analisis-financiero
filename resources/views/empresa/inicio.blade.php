@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if($empresas->isNotEmpty())
            <div class=" d-flex justify-content-end">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#NuevaEmpresaModal">
                    Registrar Nueva Empresa
                </button>
            </div>
            <br>
            @foreach ($empresas as $empresa)
            <div class="card">
                <div class="card-header">
                    Empresa: {{ $empresa->titulo }}
                </div>
                <div class="card-body">
                    {{ $empresa->descripcion }}
                    <div class="d-flex justify-content-end">
                        <a class="btn btn-outline-primary" href="{{ route('empresa.detalle',$empresa->id_empresa) }}" role="button">Detalle</a>
                    </div>
                </div>
                <div class="card-footer text-muted">
                    {{ $empresa->sector }}
                </div>
            </div>
            <br>
            @endforeach

            @else
            <div class="card">
                <div class="card-header">
                    Registrar nueva empresa
                </div>
                <div class="card-body">
                    Agrega una nueva empresa para utilizar nuestra herramienta de analisis financieros
                </div>
                <div class="card-footer text-muted">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#NuevaEmpresaModal">
                        Registrar
                    </button>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>

<!-- ADD Modal HTML -->
<div id="NuevaEmpresaModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{route('empresa.store')}}" method="post" id="empresaform">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">Agregar Clasificación</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        {{-- Nombre --}}
                        <label>Nombre de la empresa <sup><i class='fas fa-asterisk' style='font-size:6px;color:red'></i></sup></label>
                        <input minlength="6" type="text" class="form-control {{$errors->has('nombre') ? 'is-invalid' : ''}}" id="nombre" name="nombre" required="" oninvalid="this.setCustomValidity('Este campo es requerido')" oninput="setCustomValidity('')">
                        @if($errors->has('nombre'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                        @endif
                        {{-- Descripcion --}}
                        <label>Descripcion de la empresa <sup><i class='fas fa-asterisk' style='font-size:6px;color:red'></i></sup></label>
                        <textarea minlength="6" class="form-control {{$errors->has('descripcion') ? 'is-invalid' : ''}}" id="descripcion" name="descripcion" rows="3" required="" oninvalid="this.setCustomValidity('Este campo es requerido')" oninput="setCustomValidity('')"></textarea>
                        @if($errors->has('descripcion'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('descripcion') }}</strong>
                        </span>
                        @endif
                        {{-- Select Sector --}}
                        <label class="mt-3">Sector</label>
                        <select name="sector" class="form-control {{$errors->has('sector') ? 'is-invalid' : ''}}" id="sector">
                            <option value="">Selecionar sector</option>
                            @if ($sectores)
                            @foreach ($sectores as $sector)
                            <option value="{{ $sector->id_sector }}" {{(old('sector') == $sector->id_sector) ? 'selected="true"' : ''}}>
                                {{ $sector->nombre }}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Registrar</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection