@extends('layouts.app')

@section('librerias')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.15.3/dist/sweetalert2.all.min.js"></script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    Empresa : {{ $empresa->nombre }}
                </div>
                <div class="card-body">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Lista de Balance Generales</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Lista de Estados Resultados</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="d-flex justify-content-end">
                                <a class="btn btn-primary mt-2" href="{{ route('estado.agregar',$empresa->id_empresa) }}" role="button">Agregar Estado Resultado</a>
                            </div>
                            <br>
                            <table id="tablabalance" class="table table-sm table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col" width="20%">Año</th>
                                        <th scope="col" width="20%">Balance General</th>
                                        <th scope="col" class="text-center">Accion</th>
                                        <th scope="col" width="5%" class="text-center">Info</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($balances as $balance)
                                    @if(in_array($balance,$estados))
                                    <tr>
                                        <td>{{ $balance }}</td>
                                        <td><a>Balance General</a></td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-danger btn-sm" onclick="estadofinancierodelete({{ $balance }}, {{ $empresa->id_empresa }}, 0)" data-toggle="tooltip" data-placement="bottom" title="Eliminar Balance General">

                                                <i class="las la-trash-alt" style="font-size:24px;"></i>
                                            </button>
                                        </td>
                                        <td class="text-center">
                                            <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Agregar Estado Resultado">
                                                <button class="btn btn-second btn-sm" style="pointer-events: none;" type="button" disabled>
                                                    <i class="las la-check-circle" style="font-size:24px;color:green"></i>
                                                </button>
                                            </span>
                                        </td>
                                    </tr>
                                    @else
                                    <tr>
                                        <td>{{ $balance }}</td>
                                        <td><a>Balance General</a></td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-danger btn-sm" onclick="estadofinancierodelete({{ $balance }}, {{ $empresa->id_empresa }} ,0)" data-toggle="tooltip" data-placement="bottom" title="Eliminar Balance General">
                                                <i class="las la-trash-alt" style="font-size:24px;"></i>
                                            </button>
                                        </td>
                                        <td class="text-center">
                                            <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Agregar Estado Resultado">
                                                <button class="btn btn-second btn-sm" style="pointer-events: none;" type="button" disabled>
                                                    <i class="las la-info-circle" style="font-size:24px;"></i>
                                                </button>
                                            </span>
                                        </td>
                                    </tr>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="d-flex justify-content-end">
                                <a class="btn btn-primary mt-2" href="{{ route('balance.agregar',$empresa->id_empresa) }}" role="button">Agregar Balance General</a>
                            </div>
                            <br>
                            <table id="tablabalance" class="table table-sm table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col" width="20%">Año</th>
                                        <th scope="col" width="20%">Estado Resultado</th>
                                        <th scope="col" class="text-center">Accion</th>
                                        <th scope="col" width="5%" class="text-center">Info</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($estados as $resultado)
                                    @if(in_array($resultado,$balances))
                                    <tr>
                                        <td>{{ $resultado }}</td>
                                        <td><a>Estados Resultado</a></td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-danger btn-sm" onclick="estadofinancierodelete({{ $resultado }}, {{ $empresa->id_empresa }},1)" data-toggle="tooltip" data-placement="bottom" title="Eliminar Balance General">
                                                <i class="las la-trash-alt" style="font-size:24px;"></i>
                                            </button>
                                        </td>
                                        <td class="text-center">
                                            <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Agregar Estado Resultado">
                                                <button class="btn btn-second btn-sm" style="pointer-events: none;" type="button" disabled>
                                                    <i class="las la-check-circle" style="font-size:24px;color:green"></i>
                                                </button>
                                            </span>
                                        </td>
                                    </tr>
                                    @else
                                    <tr>
                                        <td>{{ $resultado }}</td>
                                        <td><a>Estado Resultado</a></td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-danger btn-sm" onclick="estadofinancierodelete({{ $resultado }}, {{ $empresa->id_empresa }},1)" data-toggle="tooltip" data-placement="bottom" title="Eliminar Balance General">
                                                <i class="las la-trash-alt" style="font-size:24px;"></i>
                                            </button>
                                        </td>
                                        <td class="text-center">
                                            <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Agregar Balance General">
                                                <button class="btn btn-second btn-sm" style="pointer-events: none;" type="button" disabled>
                                                    <i class="las la-info-circle" style="font-size:24px;"></i>
                                                </button>
                                            </span>
                                        </td>
                                    </tr>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://unpkg.com/axios/dist/axios.min.js">
    const axios = require('axios');
</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10">
    const Swal = require('sweetalert2');
</script>

<script type="text/javascript">
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    });
    $(document).ready(function() {
        $('.table').DataTable();
    });

    function estadofinancierodelete(anio, id, valor) {
        Swal.fire({
            title: '¿Estas seguro de eliminar el registro?',
            text: "Se eliminar el registro del año ".anio,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtontext: 'Cancelar',
            confirmButtonText: 'SI, Eliminar'
        }).then(function(isConfirm) {
            if (isConfirm.value === true) {
                axios.post('{{ route('estadosfinancieros.eliminar') }}', {
                            empresa: id,
                            anio: anio,
                            operacion: valor
                        }).then(response => {
                        console.log(response.data.message);
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 1500,
                            timerProgressBar: true,
                            didOpen: (toast) => {
                                toast.addEventListener('mouseenter', Swal.stopTimer)
                                toast.addEventListener('mouseleave', Swal.resumeTimer)
                            }
                        })
                        Toast.fire({
                            icon: 'success',
                            title: response.data.message
                        }).then(function() {
                            location.reload(true);
                        });
                    })
                    .catch(error => {
                        console.log(error)
                        Swal.fire({
                            position: 'top-end',
                            icon: 'error',
                            title: response.data.message,
                            showConfirmButton: false,
                            timer: 2000
                        })
                    });
            } 
        }, function(dismiss) {
            if (dismiss === 'cancel' || dismiss === 'close') {
                // ignore
            }
        })
    }
</script>
@endsection