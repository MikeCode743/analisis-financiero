@extends('layouts.app')
@section('librerias')
<title>Captura  OCR</title>
<script src="{{ asset('js/app.js') }}" defer></script>
@endsection
@section('content')
<div id="app">
    <tabla-ocr></tabla-ocr>
</div>
@endsection