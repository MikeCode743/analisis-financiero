@extends('layouts.app')
@section('librerias')
<script src="{{ asset('js/app.js') }}" defer></script>
@endsection
@section('content')
<div id="app" class="m-0 p-0">
    <panel-navegacion dato="{{$dato}}"></panel-navegacion>
</div>
@endsection