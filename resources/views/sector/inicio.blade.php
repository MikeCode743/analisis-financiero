@extends('layouts.app')
<title>Sector</title>
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10 ">
            <h1>Sectores</h1><br>
            <table class="table">
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Descripción </th>
                    <th>Acciones</th>
                </tr>
                @foreach ($sector as $item)
                <tr>
                    <td>{{$item->id_sector}}</td>
                    <td>{{$item->nombre}}</td>
                    <td>{{$item->descripcion}}</td>
                    <td>
                        @if (Auth::user()->acceso('000'))
                        <a href="{{route('sector.editar',$item->id_sector)}}" class="btn btn-secondary">Editar</a>
                        @endif
                        <a href="{{route('sector.comparativo',$item->id_sector)}}" class="btn btn-success">Info</a>
                    </td>
                </tr>
                @endforeach
            </table>
            {{$sector->links()}}
            <!-- Button trigger modal -->

            @if (Auth::user()->acceso('000'))
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                Agregar Sector
            </button>
            @endif
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agrergar Sector</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('sector.guardar')}}" method="POST">
                <div class="modal-body">

                    @csrf

                    <div class="form-group">
                        <input type="text" name="nombre" id="nombre" class="form-control" value="{{old('nombre')}}"
                            placeholder="Nombre Sector" required>

                    </div>

                    @error('nombre')
                    <div class="alert alert-danger">
                        El nombre del sector es requerido
                    </div>
                    @enderror

                    <div class="form-group">
                        <input type="text" name="descripcion" id="descripcion" class="form-control"
                            value="{{old('descripcion')}}" placeholder="Descripcion Sector" required>

                    </div>

                    @error('descripcion')
                    <div class="alert alert-danger">
                        La descripcion del sector es requerida
                    </div>
                    @enderror

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-success btn-block">Ingresar Sector</button>

                    @if (session('agregar'))
                    <div class="alert alert-success mt-3">
                        {{session('agregar')}}
                    </div>
                    @endif
                </div>
            </form>
        </div>
    </div>
</div>
@endsection