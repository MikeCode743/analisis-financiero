@extends('layouts.app')
<title>Editar</title>
@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h3 class='text-center mb-3 pt-3'> Editar Sector: {{$sectorActualizar->nombre}}</h3>
            <form action="{{route('sector.update' , $sectorActualizar->id_sector)}}" method="POST">
                @method('PUT')
                @csrf
                <div class="form-group">
                    <input type="text" name="nombre" id="nombre" value="{{$sectorActualizar->nombre}}"
                        class="form-control" required>
                </div>
                <div class="form-group">
                    <input type="text" name="descripcion" id="descripcion" value="{{$sectorActualizar->descripcion}}"
                        class="form-control" required>
                </div>
                @if (Auth::user()->acceso('000'))
                <button type="submit" class="btn btn-secondary btn-block">Editar Sector</button><br><br><br>
                @endif
                <a href="{{route('sector.inicio')}}" class="btn btn-success"> Regresar </a>
            </form>
            @if(session('update')) <div class="alert alert-success mt-3"> {{session('update')}} </div> @endif
        </div>
    </div>
</div>
@endsection