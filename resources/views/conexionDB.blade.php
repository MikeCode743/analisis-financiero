@extends('layouts.app')
@section('librerias')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.15.3/dist/sweetalert2.all.min.js"></script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8"> @if(session('restaurar')) <div class="alert alert-success mt-3"> {{session('restaurar')}} </div> @endif
            <div class="card">
                <div class="card-header">BASE DE DATOS</div>
                <div class="card-body">
                    <h5 class="card-title">ESTADO</h5>
                    <p class="card-text">{{ $conexion }}</p>
                    @if($e ?? '') <div class="alert alert-warning" role="alert"> {{ $e ?? '' }} </div> @endif
                    @if($poblar) @endif
                </div>
                <div class="card-footer">
                    <div id="procesando" style="display:none">
                        <div  class="d-flex  align-items-center">
                            <strong>Procesando...</strong>
                            <div id="spinner" class="spinner-border ml-auto" role="status" aria-hidden="true"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="alert alert-secondary mt-2" role="alert">
                    <p class="font-weight-bold p-0 m-0">PASOS REQUERIDOS</p>
            </div>
            <div class="alert alert-secondary mt-2" role="alert">
                1 - Crear una base de datos en MYSQL
            </div>
            <div class="alert alert-secondary mt-2" role="alert">
                2 - Modificar el archivo .env.example a .env con las credenciales de la base de datos creada en el paso anterior
            </div>
        </div>
    </div>
</div>
<script src="https://unpkg.com/axios/dist/axios.min.js">
    const axios = require('axios');

</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10">
    const Swal = require('sweetalert2');

</script>
<script class="text/javascript">
    $(document).ready(function() {
        $('#procesando').hide();
        $('#restaurar').click(function() {
            $('#procesando').show();
            $("#restaurar").prop('disabled', true);
            axios.post('{{ route('restaurar') }}')
                .then(response => {
                    console.log(response.data.message);
                    const Toast = Swal.mixin({
                        toast: true
                        , position: 'top-end'
                        , showConfirmButton: false
                        , timer: 3500
                        , timerProgressBar: true
                        , didOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    })
                    $('#procesando').hide();
                    Toast.fire({
                        icon: 'success'
                        , title: response.data.message
                    }).then(function() {
                        location.reload(true);
                    });
                }).catch(function(error) {
                    console.error(error.data);
                    Swal.fire({
                        position: 'top-end'
                        , icon: 'error'
                        , title: response.data.message
                        , showConfirmButton: false
                        , timer: 2000
                    })

                })
        });
    });
</script>


@endsection
