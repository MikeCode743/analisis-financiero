@extends('layouts.app')
<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<title> Ratios comparativos </title>
@section('content')

<div class="container">
    <h2 class='text-center mb-3 pt-1'>Ratios comparativos </h2> <br>
    @if (session('guardado'))
    <div class="alert alert-success mt-3">
        {{session('guardado')}}
    </div>
    @endif

    @if (session('error'))
    <div class="alert alert-danger mt-3">
        {{session('error')}}
    </div>
    @endif
    <div id="alert" class="alert alert-danger" role="alert">
        Debe revisar los errores!!
    </div>
    <div class="row">
        <div class="col-md-12">
            <form action="comparativo/agregar" method="POST">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="d-flex">
                            <a href="{{route('comparativo.inicio')}}" class="btn btn-primary mb-2">Volver</a><br>
                            <button type="submit" class="btn btn-primary mb-2 ml-3">
                                Agregar Ratio Comparativo
                            </button>
                        </div>
                        @csrf
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Sector:</label>
                                    <select class="form-control" name="id_sector">                                        
                                        @foreach ($sector as $item)
                                        <option value="{{$item->id_sector}}">{{$item->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Año</label>
                                    <input id="anio" type="text" name="anio" minlength="4" maxlength="4" class="form-control" placeholder="Año">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Descripcion</label>
                                    <input id="desc" type="text" name="descripcion" class="form-control" placeholder="Descripción">
                                </div>
                            </div>
                        </div>
                        <div class="row" id="num">
                            @foreach($ratio as $item)
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>{{ $item->nombre }}</label>
                                    <input value="0" type="text" name="{{$item->id_ratio_base}}" class="form-control" placeholder="Numérico">
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $("#alert").hide();

        //Submit formulario
        $("form").submit(function(e) {
            if ($("input").hasClass("is-invalid")) {
                $("#alert").show();
                e.preventDefault();
                return
            }
            if ($("#anio").val() == '') {
                $("#alert").show();
                $("#anio").addClass("is-invalid");
                e.preventDefault();
                return
            };
            if (!$("#desc").val()) {
                $("#alert").show();
                $("#desc").addClass("is-invalid");
                e.preventDefault();
                return
            };
        });

        //Agregar error al campo de los ratios
        $('#num div.col-md-3 div.form-group input.form-control').each(function() {
            $(this).keyup(function() {
                if (parseFloat($(this).val()) != $(this).val() || Math.sign($(this).val()) == -1) {
                    $(this).addClass("is-invalid");
                } else {
                    $(this).removeClass("is-invalid");
                }
                if ($(this).val() == '') {
                    $(this).removeClass("is-invalid");
                }
            });
        });

        //Agregar error al campo de los ratios
        $("#desc").keyup(function() {
            if ($(this).val() == '') {
                $(this).addClass("is-invalid");
            } else {
                $(this).removeClass("is-invalid");
            }
            if ($(this).val() == '') {
                $(this).addClass("is-invalid");
            }
        });

        $("#anio").keyup(function() {
            if (parseInt($(this).val()) != parseInt($(this).val())) {
                $(this).addClass("is-invalid");
            } else {
                $(this).removeClass("is-invalid");
            }
            if ($(this).val() == '') {
                $(this).addClass("is-invalid");
            }
        });
    });
</script>
<!-- Modal -->

@endsection