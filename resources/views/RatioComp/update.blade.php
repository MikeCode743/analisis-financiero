@extends('layouts.app')
<title> Edit Comparativo </title>
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h3 class='text-center mb-3 pt-3'> Editar Valores Ratio Comparativo {{$ratiocomp->id_comparativo}}</h3>
            <form action="{{route('comparativo.update' , $ratiocomp->id_comparativo)}}" method="POST">
                @method('PUT')
                @csrf
                <div class="form-group">
                    <input type="number" step="any" min="0" max="10" name="valor" id="valor" value="{{$ratiocomp->valor}}" class="form-control" required>
                </div>
                @if (Auth::user()->acceso('000'))
                <button type="submit" class="btn btn-secondary btn-block ">Editar Valores del Ratio</button><br><br>
                @endif
                <a href="{{route('comparativo.inicio')}}" class="btn btn-success "> Regresar </a>
            </form>
            @if(session('updatecomp')) <div class="alert alert-success mt-3"> {{session('updatecomp')}} </div> @endif
        </div>
    </div>
</div>


@endsection
