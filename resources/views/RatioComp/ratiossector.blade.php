@extends('layouts.app')
<title> Ratios del sector </title>
@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-10">
            <h3 class='text-center mx-3'> Ratios del sector {{ $sector->nombre }} </h3>
            <div class="d-flex justify-content-between p-0">
                <a href="{{route('sector.inicio')}}" class="btn btn-success"> Regresar </a>
                {{$ratioSector->links()}}
            </div>
            <div class="row mt-3">
                <div class="col-md-12">
                    <table class="table">
                        <tr>
                            <th>Ratio </th>
                            <th>Año</th>
                            <th>Valor</th>
                        </tr>
                        @foreach ($ratioSector as $item)
                        <tr>
                            <td>{{$item->nombre}}</td>
                            <td>{{$item->anio}}</td>
                            <td>{{$item->valor}}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection