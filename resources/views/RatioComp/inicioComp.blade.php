@extends('layouts.app')

<title> Ratios comparativos </title>
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h2>Ratios comparativos </h2>
            <div class="d-flex justify-content-between">
                @if(Auth::user()->acceso('000'))
                <a href="{{route('comparativo.guardar')}}" class="btn btn-primary mb-2"> Agregar ratios </a>
                @endif
                {{$ratiocomp->links()}}
            </div>
            @if(session('eliminar')) <div class="alert alert-success mt-3"> {{session('eliminar')}} </div> @endif
            <table class="table">
                <tr>
                    <th>Sector</th>
                    <th>Ratio</th>
                    <th>Valor</th>
                    <th>Año</th>
                    <th>Descripcion</th>
                    @if (Auth::user()->acceso('000'))
                    <th>Acciones</th>
                    @endif
                </tr>
                @foreach ($ratiocomp as $item)
                <tr>
                    <td>{{$item->sector}}</td>
                    <td>{{$item->nombre}}</td>
                    <td>{{$item->valor}}</td>
                    <td>{{$item->anio}}</td>
                    <td>{{$item->descripcion}}</td>
                    @if (Auth::user()->acceso('000'))
                    <td>
                        <a href="{{route('comparativo.edit', $item->id_comparativo)}}" class="btn btn-secondary"> Editar </a>
                    </td>
                    @endif
                </tr>
                @endforeach
            </table>
            @if(Auth::user()->acceso('000'))
            <form action="{{route ('comparativo.eliminar', [$ratiocomp[0]->sector, $ratiocomp[0]->anio ,$ratiocomp[0]->descripcion] )}}" method="POST" class="d-inline">
                @method('DELETE')
                @csrf
                <button type="submit" class="btn btn-danger">Eliminar</button>
            </form>
            @endif
        </div>
    </div>
</div>


@endsection