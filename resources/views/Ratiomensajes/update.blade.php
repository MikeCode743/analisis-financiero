@extends('layouts.app')
<title> Editar Mensaje </title>
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <h3 class='text-center mb-3 pt-3'> Editar Mensaje: {{$mensajeratio->ratio->nombre}}</h3>
            <form action="{{route('mensaje.update' , $mensajeratio->id_mensaje)}}" method="POST">
                @method('PUT')
                @csrf
                <div class="form-group">
                    <label>Mensaje Mayor</label>
                    <input type="text" name="mayor" id="mayor" value="{{$mensajeratio->mayor}}" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Mensaje Menor</label>
                    <input type="text" name="menor" id="menor" value="{{$mensajeratio->menor}}" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Mensaje Igual</label>
                    <input type="text" name="igual" id="igual" value="{{$mensajeratio->igual}}" class="form-control" required>
                </div>
                @if (Auth::user()->acceso('000'))
                <button type="submit" class="btn btn-secondary btn-block">Editar Mensaje</button>
                @endif
                <br><br><br>
                <a href="{{route('mensaje.inicio')}}" class="btn btn-success "> Regresar </a>
            </form>
            @if(session('updateMensaje')) <div class="alert alert-success mt-3"> {{session('updateMensaje')}} </div> @endif
        </div>
    </div>
</div>



@endsection