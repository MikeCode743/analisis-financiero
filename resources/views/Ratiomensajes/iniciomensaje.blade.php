@extends('layouts.app')
<title> Mensajes </title>
@section('content')

<div class="container">
    <h1>Mensajes Almacenados</h1>
    @if (session('guardar')) <div class="alert alert-success mt-3"> {{session('guardar')}} </div> @endif
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-4">
                @if (Auth::user()->acceso('000'))
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Agregar Mensajes
                </button>
                @endif
            </div>
            <div class="col-3">
                {{$ratiomensaje->links()}}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <tr>
                    <th>ID</th>
                    <th>Ratio Base</th>
                    <th>Mensaje Mayor</th>
                    <th>Mensaje Menor</th>
                    <th>Mensaje Igual</th>
                    @if (Auth::user()->acceso('000'))
                    <th>Acciones</th>
                    @endif
                </tr>
                @foreach ($ratiomensaje as $item)
                <tr>
                    <td>{{$item->id_mensaje}}</td>
                    <td>{{$item->ratio->nombre}}</td>
                    <td>{{$item->mayor}}</td>
                    <td>{{$item->menor}}</td>
                    <td>{{$item->igual}}</td>
                    @if (Auth::user()->acceso('000'))
                    <td>
                        <a href="{{route('mensaje.edit', $item->id_mensaje)}}" class="btn btn-warning"> Editar </a>
                    </td>
                    @endif

                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agrergar Mensaje</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('mensaje.guardar')}}" method="POST">
                <div class="modal-body">
                    <div class="form-group">
                        @csrf
                        <div class="form-group">
                            <label for="">Ratio Base</label>
                            <select class="form-control" name="id_ratio_base">
                                @foreach ($ratio as $item)
                                <option value="{{$item->id_ratio_base}}">{{$item->nombre}}</option>
                                @endforeach

                                @error('id_ratio_base')
                                <div class="alert alert-danger">
                                    El ratio base es requerido
                                </div>
                                @enderror
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" name="mayor" id="mayor" class="form-control" value="{{old('mayor')}}" placeholder="Mensaje si ratio es mayor a promedio" required>
                        </div>
                        @error('mayor') <div class="alert alert-danger"> El mensaje es requerido </div> @enderror
                        <div class="form-group">
                            <input type="text" name="menor" id="menor" class="form-control" value="{{old('menor')}}" placeholder="Mensaje si el ratio es menor que el promedio" required>
                        </div>
                        @error('menor') <div class="alert alert-danger"> El mensaje si el ratio es menor, es requerido
                        </div> @enderror
                        <div class="form-group">
                            <input type="text" name="igual" id="igual" class="form-control" value="{{old('igual')}}" placeholder="Mensaje si el ratio es igual que el promedio" required>
                        </div>
                        @error('igual') <div class="alert alert-danger"> El mensaje si el ratio es igual, es requerido
                        </div> @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-success btn-block">Ingresar Mensajes</button>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection