<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous">
    </script>

    @section('elementvue')
    @show
</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light mx-4">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/welcome">Inicio<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Administración
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="/ratio">Ratio</a>
                        <a class="dropdown-item" href="/ratio/grupo">Ratio Grupo</a>
                        <a class="dropdown-item" href="/ratio/mensaje">Ratio Mensaje</a>
                        <a class="dropdown-item" href="/ratio/comparativo">Ratio Comparativo</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="/sector">Sector</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/navegacion">Panel de control</a>
                </li>
            </ul>
        </div>
    </nav>

    @section('container')
    @show

</body>
<script>
    document.getElementById("navbarDropdown").addEventListener("mouseover", function() {
        document.getElementById("navbarDropdown").classList.add("show");
        document.getElementsByClassName("dropdown-menu")[0].classList.add("show");
    })

    document.getElementsByClassName("dropdown-menu")[0].addEventListener("mouseover", function() {
        document.getElementById("navbarDropdown").classList.add("show");
        document.getElementsByClassName("dropdown-menu")[0].classList.add("show");
    })

    document.getElementsByClassName("dropdown-menu")[0].addEventListener("mouseout", function() {
        document.getElementById("navbarDropdown").classList.remove("show");
        document.getElementsByClassName("dropdown-menu")[0].classList.remove("show");
    })
</script>

</html>