<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClasificacionRazon extends Model
{
    protected $table = 'clasificacionrazon';
    protected $primaryKey = 'id_clasificacion_razon';
    public $timestamps = false;
}
