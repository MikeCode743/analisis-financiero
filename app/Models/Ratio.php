<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ratio extends Model
{
   protected $table = 'ratio';
   protected $primaryKey = 'id_ratio_base';
   public $timestamps = false;

   public function ratiomensaje()
   {
      return $this->belongsTo('App\Models\RatioMensaje');
   }

   public function ratiocomparativo()
   {
      return $this->belongsTo('App\Models\RatioComparativo', 'id_comparativo');
   }
}
