<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RatioDetalle extends Model
{
    protected $table = 'ratiodetalle';
    protected $primaryKey = 'id_ratio_detalle';
    public $timestamps = false;
}
