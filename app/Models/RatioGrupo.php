<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RatioGrupo extends Model
{
    protected $table = 'ratiogrupo';
    protected $primaryKey = 'id_ratio_grupo';
    public $timestamps = false;
}
