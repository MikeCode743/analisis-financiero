<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OpcionFormulario extends Model
{
    protected $table = 'opcionformulario';
    public $timestamps = false;
    protected $fillable = ['id_opcion','descripcion_opcion','numero_formulario'];
}
