<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstadoResultadoBase extends Model
{
    protected $table = 'estadoresultadobase';
    protected $primaryKey = 'id_estado_base';
    public $timestamps = false;
}
