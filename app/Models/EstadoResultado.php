<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstadoResultado extends Model
{
    protected $table = 'estadoresultado';
    protected $primaryKey = 'id_estado';
    public $timestamps = false;
}
