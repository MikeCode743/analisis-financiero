<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RatioMensaje extends Model
{
    protected $table = 'ratiomensaje';
    protected $primaryKey = 'id_mensaje';
    public $timestamps = false;

    public function ratio()
    {
        return $this->belongsTo('App\Models\Ratio', 'id_ratio_base');
    }
}
