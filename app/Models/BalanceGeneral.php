<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BalanceGeneral extends Model
{
    protected $table = 'balancegeneral';
    protected $primaryKey = 'id_balance';
    public $timestamps = false;
}
