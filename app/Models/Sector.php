<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
    protected $table = 'sector';
    protected $primaryKey = 'id_sector';
    public $timestamps = false;

    public function ratiocomparativo(){
       
        return $this->belongsTo('App\Models\RatioComparativo', 'id_comparativo' ); 
    }
}
