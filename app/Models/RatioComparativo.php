<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RatioComparativo extends Model
{
    protected $table = 'ratiocomparativo';
    protected $primaryKey = 'id_comparativo';
    public $timestamps = false;

    public function sector()
    {
        return $this->belongsTo('App\Models\Sector', 'id_sector');
    }
    public function ratio()
    {
        return $this->hasMany('App\Models\Ratio', 'id_ratio_base');
    }
}
