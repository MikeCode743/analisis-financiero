<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BalanceGeneralBase extends Model
{    
    protected $table = 'balancegeneralbase';
    protected $primaryKey = 'id_balance_base';
    public $timestamps = false;
}
