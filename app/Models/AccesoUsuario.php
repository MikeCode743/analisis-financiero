<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccesoUsuario extends Model
{
    protected $table = 'accesousuario';
    public $timestamps = false;
    protected $fillable = ['id_usuario','id_empresa','id_opcion'];
}
