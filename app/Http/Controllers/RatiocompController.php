<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sector;
use App\Models\Ratio;
use App\Models\RatioComparativo;
use Illuminate\Support\Facades\DB;

class RatiocompController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*----- ACCESO -----------*/
        // Auth::user()->acceso('015');
        /*----------------------*/

        $ratio = Ratio::all();
        $size = count($ratio);
        $ratiocomp = DB::table('ratiocomparativo')
            ->join('ratio', 'ratiocomparativo.id_ratio_base', '=', 'ratio.id_ratio_base')
            ->join('sector', 'ratiocomparativo.id_sector', '=', 'sector.id_sector')
            ->select('sector.nombre AS sector', 'ratio.*', 'ratiocomparativo.*')
            ->orderBy('ratiocomparativo.id_comparativo')
            ->paginate($size);
        $sector = Sector::all();
        return view('RatioComp/inicioComp',  compact('ratiocomp', 'ratio', 'sector'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ratiocompAgregar = new RatioComparativo;
        $request->validate([
            'id_sector' => 'required',
            'id_ratio_base' => 'required',
            'valor' => 'required',
            'anio' => 'required',
        ]);
        $ratiocompAgregar->id_sector = $request->id_sector;
        $ratiocompAgregar->id_ratio_base = $request->id_ratio_base;
        $ratiocompAgregar->valor = $request->valor;
        $ratiocompAgregar->anio = $request->anio;
        $ratiocompAgregar->save();
        return back()->with('almacenar', 'El ratio comparativo ha sido agregado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_comparativo)
    {
        $ratiocomp = RatioComparativo::findOrFail($id_comparativo);
        return view('RatioComp/update', compact('ratiocomp'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_comparativo)
    {
        $ratioupdate = RatioComparativo::findOrFail($id_comparativo);
        $ratioupdate->valor = $request->valor;        
        $ratioupdate->save();
        return back()->with('updatecomp', 'Los valores han sido actualizados');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($sector, $anio, $desc)
    {
        $id_sector = Sector::where('nombre', $sector)->select('id_sector')->get()[0]['id_sector'];
        RatioComparativo::where('id_sector', $id_sector)->where('anio', $anio)->where('descripcion', $desc)->delete();        
        return redirect()->route('comparativo.inicio');
    }

    public function cargarRatio()
    {
        $ratio = Ratio::all();
        $sector = Sector::all();
        return view('RatioComp/agregarratio', compact('ratio', 'sector'));
    }

    public function agregarRatio(Request $request)
    {
        $nuevoratio = collect($request)
            ->forget('_token')
            ->forget('anio')
            ->forget('descripcion')
            ->forget('id_sector');

        $ratiocomp = RatioComparativo::where('anio', $request->anio)
            ->where('descripcion', $request->descripcion)->where('id_sector', $request->id_sector)
            ->doesntExist();

        if ($ratiocomp) {
            foreach ($nuevoratio as $item => $valor) {
                $ratio = new Ratio;
                $ratio->id_sector = $request->id_sector;
                $ratio->anio = $request->anio;
                $ratio->descripcion = $request->descripcion;
                $ratio->id_ratio_base = $item;
                $ratio->valor = $valor;
                DB::table('ratiocomparativo')->insert($ratio->toArray());
            }
            return back()->with('guardado', 'guardado con exito.');
        }
        return back()->with('error', 'Valores duplicados en año y descripcion');
    }

    public function enviosector($id_sector)
    {
        $sector = Sector::findorFail($id_sector);
        $ratio = Ratio::all();
        $size = count($ratio);
        $ratioSector = DB::table('ratiocomparativo')
            ->join('ratio', 'ratiocomparativo.id_ratio_base', '=', 'ratio.id_ratio_base')
            ->where('id_sector', '=', $id_sector)
            ->orderBy('anio')
            ->paginate($size);
        return view('RatioComp/ratiossector', compact('ratioSector', 'sector'));
    }
}
