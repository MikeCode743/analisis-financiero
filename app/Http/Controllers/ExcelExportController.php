<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\CollectionExport;
use Exception;
use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Support\Facades\Storage;

class ExcelExportController extends Controller
{
    public function exportar(Request $request)
    {
    	try{
	    	$datosExportar = $request->json()->all();
	        $nombreArchivo = 'archivo.xlsx';

	        $export = new CollectionExport($datosExportar);//->toArray()

	        Excel::store($export, $nombreArchivo, 'public');

	        $rutaArchivoExportar = Storage::disk('public')->path($nombreArchivo);
	        return response()->json(['ruta' => asset(Storage::url($nombreArchivo))]);
	        //return response()->json(['ruta' => asset('storage/' . $nombreArchivo)]);        
	    }catch(Exception $e){
	    	echo 'Excepción capturada: ',  $e->getMessage(), "\n";
			return response()->json([ "error" => "Error al exportar."]);
	    }
    }
}
