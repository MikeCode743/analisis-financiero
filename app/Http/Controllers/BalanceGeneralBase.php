<?php

namespace App\Http\Controllers;

use App\Http\Resources\BalanceGeneralBase as ResourcesBalanceGeneralBase;
use App\Models\BalanceGeneral;
use App\Models\BalanceGeneralBase as balancebase;
use Illuminate\Http\Request;
use App\Models\Empresa;
use Illuminate\Support\Facades\DB;

use App\Imports\ExcelImport;
use Exception;
use Illuminate\Support\Facades\Auth;

class BalanceGeneralBase extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function show($id)
    {
        /*----- ACCESO -----------*/
        // Auth::user()->acceso('0101');
        /*----------------------*/
        $empresa = Empresa::FindOrFail($id);
        return view('empresa.estructuraBalance', ['id' => $id, 'empresa' => $empresa, 'importado' => false]);
    }

    public function show2($id)
    {
        /*----- ACCESO -----------*/
        // Auth::user()->acceso('020');
        /*----------------------*/
        $empresa = Empresa::FindOrFail($id);
        return view('empresa.agregarBalance', ['id' => $id, 'empresa' => $empresa, 'importado' => false]);
    }


    //API
    public function store(Request $request)
    {
        $id = $request->id;
        $catalogo = $request->catalogo;

        if (DB::table('balancegeneralbase')->where('id_empresa', $id)->doesntExist()) {
            foreach ($catalogo as $c) {
                $cuenta = new balancebase;
                $cuenta->id_empresa = $id;
                $cuenta->nomenclatura = $c["catalogo"];
                $cuenta->nombre_cuenta = $c["nombre"];
                $cuenta->identificador = $c["id"];
                $cuenta->save();
            }
            return response()->json(["mensaje" => "Guardado con exito"]);
        } else {
            return response()->json(["mensaje" => "Balance General configurado anteriormente"]);
        }
    }

    public function mostrarBase($id)
    {
        Empresa::FindOrFail($id);
        if (DB::table('balancegeneralbase')->where('id_empresa', $id)->doesntExist()) {
            return response()->json(["mensaje" => "No existe la empresa"], 404);
        }
        return ResourcesBalanceGeneralBase::collection(balancebase::where('id_empresa', $id)->get());
    }

    public function guardarBalance(Request $request)
    {
        $id = $request->id;
        $catalogo = $request->catalogo;
        $anio = $request->anio;

        if (DB::table('balancegeneral')
            ->join('balancegeneralbase', 'balancegeneral.id_balance_base', '=', 'balancegeneralbase.id_balance_base')
            ->where('id_empresa', $id)
            ->where('anio', $anio)
            ->doesntExist()
        ) {
            foreach ($catalogo as $c) {
                $cuenta = new BalanceGeneral;
                $cuenta->id_balance_base = $c["id_balance_base"];
                $cuenta->anio = $anio;
                $cuenta->valor = $c["valor"];
                $cuenta->save();
            }
            $mensaje = "Nuevo Balance General " . $anio;
            return response()->json(["mensaje" => $mensaje, "id" => 0]);
        }

        $mensaje = "Ya existe registro " . $anio;
        return response()->json(["mensaje" => $mensaje, "id" => 1]);
    }

    public function listabalances($id)
    {
        try {
            //code...
            $empresa = Empresa::FindOrFail($id);
            /*----- ACCESO -----------*/
            Auth::user()->empresa($id);
            /*----------------------*/

            $balances = DB::table('balancegeneral')
                ->join('balancegeneralbase', 'balancegeneral.id_balance_base', '=', 'balancegeneralbase.id_balance_base')
                ->orderBy('anio', 'asc')
                ->select('anio')
                ->where('id_empresa', $id)
                ->groupBy('anio')
                ->pluck('anio')
                ->toArray();

            $estados = DB::table('estadoresultadobase')
                ->join('estadoresultado', 'estadoresultado.id_estado_base', '=', 'estadoresultadobase.id_estado_base')
                ->select('anio')
                ->orderBy('anio', 'asc')
                ->where('id_empresa', $id)
                ->groupBy('anio')
                ->pluck('anio')
                ->toArray();

            return view('empresa.estadosfinancieros', [
                'balances' => $balances,
                'estados' => $estados,
                'empresa' => $empresa
            ]);
        } catch (\exception $e) {
        }
    }

    public function eliminarbalance(Request $request)
    {

        $id = $request->empresa;
        $anio = $request->anio;
        $accion = $request->operacion;
        if ($accion == 0) {
            try {
                DB::table('balancegeneral')
                    ->join('balancegeneralbase', 'balancegeneral.id_balance_base', '=', 'balancegeneralbase.id_balance_base')
                    ->where('balancegeneralbase.id_empresa', $id)
                    ->where('balancegeneral.anio', $anio)
                    ->delete();
                return response()->json(['message' => 'Eliminado con éxito']);
            } catch (Exception $e) {
                return response()->json(['message' => 'No se puedo eliminar el registro'], 500);
            }
        }
        if ($accion == 1) {
            try {
                DB::table('estadoresultado')
                    ->join('estadoresultadobase', 'estadoresultado.id_estado_base', '=', 'estadoresultadobase.id_estado_base')
                    ->where('estadoresultadobase.id_empresa', $id)
                    ->where('estadoresultado.anio', $anio)
                    ->delete();
                return response()->json(['message' => 'Eliminado con éxito']);
            } catch (Exception $e) {
                return response()->json(['message' => 'No se puedo eliminar el registro'], 500);
            }
        }
    }
}
