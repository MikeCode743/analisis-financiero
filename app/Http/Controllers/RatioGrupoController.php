<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RatioGrupo;
use Illuminate\Support\Facades\Auth;

class RatioGrupoController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function show(){

		/*----- ACCESO -----------*/
		Auth::user()->opciones('016');
        /*----------------------*/
		
		$ratiosGrupo = RatioGrupo::get();

		return view('empresa.agregarRatioGrupo', ["listaRatioGrupo" => json_encode($ratiosGrupo)]);
	}

	public function store(Request $request){
		$catalogoTemp = explode(",", $request->catalogo);

		$ratioGrupo = new RatioGrupo();
		$ratioGrupo->nombre = $request->nombre;
		$ratioGrupo->catalogo = json_encode($catalogoTemp);
		$ratioGrupo->descripcion = $request->descripcion;

		$ratioGrupo->save();
		//dd($ratioGrupo->$id_ratio_grupo);
		return response()->json(["id" => $ratioGrupo->id_ratio_grupo]);
	}

	public function update(Request $request){
		$ratioGrupo = RatioGrupo::findOrFail($request->id_ratio_grupo);

		$catalogoTemp = explode(",", $request->catalogo);
				
		$ratioGrupo->nombre = $request->nombre;
		$ratioGrupo->catalogo = json_encode($catalogoTemp);
		$ratioGrupo->descripcion = $request->descripcion;

		$ratioGrupo->update();
		//echo $ratioGrupo->id_ratio_grupo;
		return response("status ok", 200);
	}

}