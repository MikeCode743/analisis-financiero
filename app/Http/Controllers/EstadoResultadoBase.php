<?php

namespace App\Http\Controllers;

use App\Http\Resources\EstadoResultadoBase as ResourcesEstadoResultadoBase;
use App\Models\EstadoResultadoBase as resultadobase;
use App\Models\EstadoResultado as resultado;
use Illuminate\Http\Request;
use App\Models\Empresa;
use Illuminate\Support\Facades\DB;
use App\Imports\ExcelImport;
use Exception;
use Illuminate\Support\Facades\Auth;

class EstadoResultadoBase extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth');
    }
    
    public function show($id)
    {
        /*----- ACCESO -----------*/
        // Auth::user()->acceso('011');
        /*----------------------*/
        $empresa = Empresa::FindOrFail($id);
        return view('empresa.estructuraEstado', ['id' => $id, 'empresa' => $empresa]);
    }
    public function show2($id)
    {
        /*----- ACCESO -----------*/
        // Auth::user()->acceso('021');
        /*----------------------*/
        $empresa = Empresa::FindOrFail($id);
        return view('empresa.agregarEstado', ['id' => $id, 'empresa' => $empresa]);
    }

    //API
    public function store(Request $request)
    {
        $id = $request->id;
        $catalogo = $request->catalogo;
        if (DB::table('estadoresultadobase')->where('id_empresa', $id)->doesntExist()) {
            foreach ($catalogo as $c) {
                $cuenta = new resultadobase;
                $cuenta->id_empresa = $id;
                $cuenta->nomenclatura = $c["nomenclatura"];
                $cuenta->nombre_cuenta = $c["nombre"];
                $cuenta->identificador = $c["id"];
                $cuenta->calculado = $c["calculado"];
                $cuenta->decremento = $c["decremento"];
                $cuenta->calculado_configurado = json_encode($c["calculado_configurado"]);
                $cuenta->save();
            }
            return response()->json(["mensaje" => "Guardado con exito"]);
        } else {
            return response()->json(["mensaje" => "Estado de Resultado configurado anteriormente"]);
        }
    }

    public function mostrarBase($id)
    {
        Empresa::FindOrFail($id);
        if (DB::table('estadoresultadobase')->where('id_empresa', $id)->doesntExist()) {
            return response()->json(["mensaje" => "No existe la empresa"], 404);
        }
        return ResourcesEstadoResultadoBase::collection(resultadobase::where('id_empresa', $id)->get());
    }

    public function guardarEstado(Request $request)
    {
        $id = $request->id;
        $catalogo = $request->catalogo;
        $anio = $request->anio;
        if (DB::table('estadoresultado')
            ->join('estadoresultadobase', 'estadoresultadobase.id_estado_base', '=', 'estadoresultado.id_estado_base')
            ->where('id_empresa', $id)
            ->where('anio', $anio)
            ->doesntExist()
        ) {
            foreach ($catalogo as $c) {
                $cuenta = new resultado;
                $cuenta->id_estado_base = $c["id_estado_base"];
                $cuenta->anio = $anio;
                $cuenta->valor = $c["valor"];
                $cuenta->save();
            }
            $mensaje = "Nuevo Estado de Resultado " . $anio;
            return response()->json(["mensaje" => $mensaje, "id" => 0]);
        } else {
            $mensaje = "Ya existe registro " . $anio;
            return response()->json(["mensaje" => $mensaje, "id" => 1]);
        }
    }

    public function eliminarEstadoResultado(Request $request)
    {
        $id = $request->empresa;
        $anio = $request->anio;
        try {
            DB::table('estadoresultado')
                ->join('estadoresultadobase', 'estadoresultado.id_estado_base', '=', 'estadoresultadobase.id_estado_base')
                ->where('estadoresultadobase.id_empresa', $id)
                ->where('estadoresultado.anio', $anio)
                ->delete();
            return response()->json(['message' => 'Eliminado con éxito'], 200);
        } catch (Exception $e) {
            return response()->json(['message' => 'No se puedo eliminar el registro'], 500);
        }
    }
    public function listaEstadosResultados($id)
    {
        $empresa = Empresa::FindOrFail($id);

        if ($empresa) {
            $estadosesultados = DB::table('estadoresultado')
                ->join('estadoresultadobase', 'estadoresultado.id_estado_base', '=', 'estadoresultadobase.id_estado_base')
                ->select('estadoresultado.anio')
                ->where('estadoresultadobase.id_empresa', $id)
                ->groupBy('estadoresultado.anio')
                ->get();
        }
        return view('empresa.listaEstadosResultados', [
            'estadosesultados' => $estadosesultados,
            'empresa' => $empresa
        ]);
    }
}
