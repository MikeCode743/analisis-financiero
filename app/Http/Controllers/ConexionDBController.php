<?php

namespace App\Http\Controllers;

use Dotenv\Result\Result;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use PDO;

class ConexionDBController extends Controller
{
    public function Conexion()
    {
        // SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME LIKE 'wild'
        $poblar = false;
        try {
            DB::connection()->getPdo();
            $sql = "select SCHEMA_NAME from INFORMATION_SCHEMA.SCHEMATA where SCHEMA_NAME = 'DB'";
            $sql = str_replace('DB', DB::connection()->getDatabaseName(), $sql);
            if (DB::select($sql)) {
                try {
                    if(DB::table('empresa')->count()){
                        $conexion = "¡Si! Conectado con éxito a la base de datos: " . DB::connection()->getDatabaseName() . " Ya Existen Datos";
                        return view('conexionDB', compact('conexion', 'poblar'));
                    }
                } catch (\Exception $e) {
                    $conexion = "¡Si! Conectado con éxito a la base de datos: " . DB::connection()->getDatabaseName() . " pero aun no hay datos. ¿Quieres Poblar la base de datos?";
                    $poblar = true;
                    return view('conexionDB', compact('conexion', 'poblar'));
                }
            } else {
                $conexion = "No se pudo encontrar la base de datos. Comprueba tu configuración.";
                return view('conexionDB', compact('conexion', 'poblar'));
            }
        } catch (\Exception $e) {
            $conexion = "No se pudo abrir la conexión al servidor de la base de datos. Comprueba tu configuración. \n Mensaje por parte del servidor: ";
            return view('conexionDB', ['conexion' => $conexion, 'poblar' => $poblar, 'e' => $e->getMessage()]);
        }
    }

    public function restaurar(Request $request)
    {
        try {            
            $sql = str_replace(array("\r\n", "\n", "\r" ), ' ', File::get(storage_path('script.sql'))); 
            $statements = array_filter(array_map('trim', explode(';', $sql)));
            foreach ($statements as $stmt) {
                DB::statement($stmt);
            }
            $datos = str_replace(array("\r\n", "\n", "\r"), ' ', File::get(storage_path('datos.sql')));
            $insert = array_filter(array_map('trim', explode(';', $datos)));
            foreach ($insert as $item) {
                DB::statement($item);
            }
                return response()->json(['message' => 'Proceso de almacenamiento exitoso'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No se pudo restaurar la base de datos '], 500);
        }
    }

}
