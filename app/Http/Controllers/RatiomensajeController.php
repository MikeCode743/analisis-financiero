<?php

namespace App\Http\Controllers;

use App\Models\RatioMensaje;
use App\Models\Ratio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RatiomensajeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*----- ACCESO -----------*/
        // Auth::user()->acceso('014');
        /*----------------------*/
        $ratiomensaje = RatioMensaje::paginate(5);
        $ratio = Ratio::all();
        return view('Ratiomensajes/iniciomensaje',  compact('ratiomensaje', 'ratio'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'id_ratio_base' => 'required',
            'mayor' => 'required',
            'menor' => 'required',
            'igual' => 'required',
        ]);

        DB::table('ratiomensaje')
            ->updateOrInsert(
                ['id_ratio_base' => $request->id_ratio_base],
                [
                    'id_ratio_base' => $request->id_ratio_base,
                    'mayor' => $request->mayor,
                    'menor' => $request->menor,
                    'igual' => $request->igual,
                ]
            );

        return back()->with('guardar', 'El mensaje ha sido agregado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_mensaje)
    {
        $ratio = Ratio::all();
        $mensajeratio = RatioMensaje::findOrFail($id_mensaje);
        return view('Ratiomensajes/update', compact('mensajeratio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_mensaje)
    {
        RatioMensaje::findOrFail($id_mensaje);
        DB::table('ratiomensaje')
            ->where('id_mensaje', $id_mensaje)
            ->update(['menor' => $request->menor, 'igual' => $request->igual, 'mayor' => $request->mayor]);
        return back()->with('updateMensaje', 'Los mensajes han sido actualizados');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
