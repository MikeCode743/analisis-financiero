<?php

namespace App\Http\Controllers;

use App\Http\Resources\RatioValor;
use App\Models\Empresa;
use App\Models\RatioComparativo;
use App\Models\Sector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Ratio;
use App\Models\BalanceGeneralBase;
use App\Models\EstadoResultadoBase;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class ComparativaSector extends Controller
{

    //API obtener razon ingresada por año y dec
    public function filtroComparativo(Request $request)
    {
        $valorComparar = DB::table('ratiocomparativo')
            ->orderBy('id_ratio_base', 'asc')
            ->where('anio', $request->anio)
            ->where('descripcion', $request->descripcion)
            ->pluck('valor');;
        return response()->json($valorComparar);
    }

    public function empresaSector($idSector)
    {

        Sector::FindOrFail($idSector);

        $cantidadEmpresas = Empresa::where('id_sector', $idSector)->select('id_empresa')->pluck('id_empresa');

        if ($cantidadEmpresas->count() == 0) {
            return ("No hay empresas asociadas al sector");
        }

        $empresaFiltro = collect([]);
        foreach ($cantidadEmpresas as $empresa) {
            $anioEmpresa = $this->ultimoAnio($empresa);
            //dump($anioEmpresa);
            if (count($anioEmpresa) != 0) {
                $empresaFiltro->push(['id_empresa' => $empresa, 'anio' => $anioEmpresa->max()['anio']]);
            }
        }

        if($empresaFiltro->count() == 0){
            return 'No hay fechas en comun, falta BG o ER';
        }    

        $dato = collect([]);
        foreach ($empresaFiltro as $empresa) {
            //dump($empresa['id_empresa'], $empresa['anio']);
            $result = $this->calculoRatios($empresa['id_empresa'], $empresa['anio']);
            if ($result instanceof Collection) {            
                $nombreEmpresa = Empresa::where('id_empresa', $empresa['id_empresa'])->select('nombre')->pluck('nombre');
                $dato->push(['empresa' => $nombreEmpresa[0], 'lista' => $result]);
            }
        }

        $listaRazon = RatioComparativo::where('id_sector', $idSector)
            ->orderBy('anio', 'asc')
            ->get()->unique(function ($item) {
                return $item['anio'] . $item['descripcion'];
            });

        $ratioDisponible = collect([]);
        foreach ($listaRazon as $l) {
            $ratioDisponible->push(['anio' => $l['anio'], 'descripcion' => $l['descripcion']]);
        }

        if (count($ratioDisponible) == 0) {
            return "Debe agregar ratio comparativo";
        }

        //return response()->json($dato);
        //dato es empresa y su valor
        return view('empresa.comparacionSector', ['dato' => $dato, 'comparar' => $ratioDisponible]);
    }

    public function ultimoAnio($idEmpresa)
    {
        $listaAnioBG = DB::table('balancegeneralbase')
            ->join('balancegeneral', 'balancegeneral.id_balance_base', '=', 'balancegeneralbase.id_balance_base')
            ->orderBy('anio', 'asc')
            ->select('anio')
            ->where('id_empresa', $idEmpresa)
            ->distinct()
            ->pluck('anio');
        $listaAnioER = DB::table('estadoresultadobase')
            ->join('estadoresultado', 'estadoresultado.id_estado_base', '=', 'estadoresultadobase.id_estado_base')
            ->orderBy('anio', 'asc')
            ->select('anio')
            ->where('id_empresa', $idEmpresa)
            ->distinct()
            ->pluck('anio');

        $listaAnio = collect([]);
        foreach ($listaAnioBG as $l) {
            if ($listaAnioER->contains($l)) {
                $listaAnio->push(['anio' => $l]);
            }
        }
        return $listaAnio;
    }
    //Parte Daniel
    public function calculoRatios($idEmpresa, $anio)
    {
        $listaAnioBG = DB::table('balancegeneralbase')
            ->join('balancegeneral', 'balancegeneral.id_balance_base', '=', 'balancegeneralbase.id_balance_base')
            ->orderBy('anio', 'asc')
            ->select('anio')
            ->where('id_empresa', $idEmpresa)
            ->distinct()
            ->pluck('anio');

        $listaAnioER = DB::table('estadoresultadobase')
            ->join('estadoresultado', 'estadoresultado.id_estado_base', '=', 'estadoresultadobase.id_estado_base')
            ->orderBy('anio', 'asc')
            ->select('anio')
            ->where('id_empresa', $idEmpresa)
            ->distinct()
            ->pluck('anio');

        $ratioConfigurado = DB::table('ratiodetalle')->where('id_empresa', $idEmpresa)
            ->count();

        $ratio =  DB::table('ratio')
            ->count();

        $data = collect([]);

        foreach ($listaAnioBG as $l) {
            if ($listaAnioER->contains($l)) {
                $data->push($l);
            }
        }
        if (!$data->contains($anio)) {
            return "Este año no tiene BG o ER";
        }

        if (count($data) == 0) {
            return ("No se pueden calcular");
        }

        if ($ratioConfigurado != $ratio) {
            return "Debe configurar los ratios";
        }

        $filtro = Ratio::join('ratiodetalle', 'ratiodetalle.id_ratio_base', '=', 'ratio.id_ratio_base')
            ->join('ratiogrupo', 'ratiogrupo.id_ratio_grupo', '=', 'ratio.id_ratio_grupo')
            ->select('ratio.nombre AS nombre_ratio', 'ratiogrupo.nombre AS nombre_grupo', 'operacion', 'campo1', 'campo2', 'campo3', 'ratio.id_ratio_base')
            ->where('ratiodetalle.id_empresa', $idEmpresa)
            ->get();

        $resultadoRetornar = collect([]);
        foreach ($filtro as $f) {
            if ($f->operacion == 1) {
                $a = $this->obtenerValor($f->campo1, $anio, $idEmpresa);
                $b = $this->obtenerValor($f->campo2, $anio, $idEmpresa);
                $r = $this->division($a, $b);
            }
            if ($f->operacion == 2) {
                $a = $this->obtenerValor($f->campo1, $anio, $idEmpresa);
                $b = $this->obtenerValor($f->campo2, $anio, $idEmpresa);
                $r = $this->multDiv($a, $b);
            }
            if ($f->operacion == 3) {
                $a = $this->obtenerValor($f->campo1, $anio, $idEmpresa);
                $b = $this->obtenerValor($f->campo2, $anio, $idEmpresa);
                $c = $this->obtenerValor($f->campo3, $anio, $idEmpresa);
                $r = $this->restaDiv($a, $b, $c);
            }
            if ($f->operacion == 4) {
                $a = $this->obtenerValor($f->campo1, $anio, $idEmpresa);
                $b = $this->obtenerValor($f->campo2, $anio, $idEmpresa);
                $c = $this->obtenerValor($f->campo3, $anio, $idEmpresa);
                $r = $this->sumaDiv($a, $b, $c);
            }
            if ($f->operacion == 5) {
                $a = $this->obtenerValor($f->campo1, $anio, $idEmpresa);
                $b = $this->obtenerValor($f->campo2, $anio, $idEmpresa);
                $r = $this->resta($a, $b);
            }
            if ($f->operacion == 6) {
                $a = $this->obtenerValor($f->campo1, $anio, $idEmpresa);
                $b = $this->obtenerValorConPromedio($f->campo2, $anio, $idEmpresa);
                $r = $this->division($a, $b);
            }
            if ($f->operacion == 7) {
                $a = $this->obtenerValorConPromedio($f->campo1, $anio, $idEmpresa);
                $b = $this->obtenerValor($f->campo2, $anio, $idEmpresa);
                $r = $this->multDiv($a, $b);
            }

            $f = collect($f)->merge(collect(['valorObtenido' => floatval(number_format($r, 2))]));
            $f = $f->forget('nombre_grupo')
                ->forget('operacion')
                ->forget('campo1')
                ->forget('campo2')
                ->forget('campo3')
                ->forget('id_ratio_base');
            $resultadoRetornar->push($f);
        }
        return $resultadoRetornar;
    }

    //Obtiene el valor de una cuenta de acuerdo a la empresa, el año y el identificador que recibe.
    public function obtenerValor($identificador, $anio, $idEmpresa)
    {
        //Hacia Estado Resultado
        if (strlen($identificador) == 3) {
            $valor = EstadoResultadoBase::join('estadoresultado as er', 'estadoresultadobase.id_estado_base', '=', 'er.id_estado_base')
                ->select('estadoresultadobase.nombre_cuenta', 'er.valor')
                ->where('estadoresultadobase.identificador', $identificador)
                ->where('er.anio', $anio)
                ->where('estadoresultadobase.id_empresa', $idEmpresa)
                ->get();
            return $valor[0]->valor;
        }

        //Hacia Balance General
        $valor = BalanceGeneralBase::join('balancegeneral as bg', 'balancegeneralbase.id_balance_base', '=', 'bg.id_balance_base')
            ->select('balancegeneralbase.nombre_cuenta', 'bg.valor')
            ->where('balancegeneralbase.identificador', $identificador)
            ->where('bg.anio', $anio)
            ->where('balancegeneralbase.id_empresa', $idEmpresa)
            ->get();
        return $valor[0]->valor;
    }

    //Calculo de valores con PROMEDIO
    public function obtenerValorConPromedio($identificador, $anio, $idEmpresa)
    {
        //Hacia Estado Resultado
        if (strlen($identificador) == 3) {
            $valor = EstadoResultadoBase::join('estadoresultado as er', 'estadoresultadobase.id_estado_base', '=', 'er.id_estado_base')
                ->where('estadoresultadobase.identificador', $identificador)
                ->where('er.anio', '<=', $anio)
                ->where('estadoresultadobase.id_empresa', $idEmpresa)
                ->avg('er.valor');
            return $valor;
        }

        //Hacia Balance General
        $valor = BalanceGeneralBase::join('balancegeneral as bg', 'balancegeneralbase.id_balance_base', '=', 'bg.id_balance_base')
            ->where('balancegeneralbase.identificador', $identificador)
            ->where('bg.anio', '<=', $anio)
            ->where('balancegeneralbase.id_empresa', $idEmpresa)
            ->avg('bg.valor');
        return $valor;
    }

    //Operacion 1
    public function division($val1, $val2)
    {
        return $val2 == 0 ?  0 : $val1 / $val2;
    }
    //Operacion 2
    public function multDiv($val1, $val2)
    {
        return $val2 == 0 ?  0 : ($val1 * 365) / $val2;
    }
    //Operacion 3
    public function restaDiv($val1, $val2, $val3)
    {
        return $val3 == 0 ?  0 : ($val1 - $val2) / $val3;
    }
    //Operacion 4
    public function sumaDiv($val1, $val2, $val3)
    {
        return $val3 == 0 ?  0 : ($val1 + $val2) / $val3;
    }
    //operacion 5
    public function resta($val1, $val2)
    {
        return $val1 - $val2;
    }
}
