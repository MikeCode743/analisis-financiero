<?php

namespace App\Http\Controllers;


use App\Http\Requests\ratioRequest;
use App\Http\Resources\RatioGrupo;
use App\Models\BalanceGeneralBase as BG;
use App\Models\EstadoResultadoBase as ER;
use App\Models\BalanceGeneralBase;
use App\Models\EstadoResultadoBase;
use App\Models\Empresa;
use App\Models\Ratio as ModelsRatio;
use App\Models\RatioComparativo;
use App\Models\RatioDetalle;
use App\Models\RatioGrupo as ModelsRatioGrupo;
use App\Models\RatioMensaje;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Exports\CollectionExport;
use Maatwebsite\Excel\Facades\Excel;

class Ratio extends Controller
{

    //exportar
    public function exportar(Request $request)
    {
        $filas = collect([
            ['name' => 'Name', 'surname' => 'Surname', 'email' => 'Email', 'twitter' => 'Twitter',],
            ['name' => 'Povilas', 'surname' => 'Korop', 'email' => 'povilas@laraveldaily.com', 'twitter' => '@povilaskorop'],
            ['name' => 'Taylor', 'surname' => 'Otwell', 'email' => 'taylor@laravel.com', 'twitter' => '@taylorotwell']
        ]);




        $balances = DB::table('balancegeneral')
            ->join('balancegeneralbase', 'balancegeneral.id_balance_base', '=', 'balancegeneralbase.id_balance_base')
            ->select('balancegeneralbase.nomenclatura', 'balancegeneralbase.nombre_cuenta', 'balancegeneral.anio', 'balancegeneral.valor')
            ->where('id_empresa', 1)
            ->where('anio', 2019)

            // ->groupBy('anio')
            ->get();

        $export = new CollectionExport($balances->ToArray());
        return Excel::download($export, 'invoices.pdf');
    }


    
    public function configurarRatio($id)
    {
        /*----- ACCESO -----------*/
        // Auth::user()->empresa($id);
        /*----------------------*/
        $empresa = Empresa::FindOrFail($id);
        return view('empresa.configurarRatio', ['id' => $id, 'empresa' => $empresa]);
    }

    //API

    //Ratio detalle
    public function configurarCuenta(Request $request)
    {
        $datoAlmacenar = $request->datos;

        foreach ($datoAlmacenar as $dato) {

            DB::table('ratiodetalle')
                ->where('id_ratio_base', $dato['id_ratio_base'])
                ->where('id_empresa', $dato['id_empresa'])
                ->delete();

            DB::table('ratiodetalle')
                ->insert($dato);
        }

        return response()->json(["mensaje" => "Razon financiera actualizada"]);
    }

    public function mostrarClasificacion($id)
    {
        Empresa::findOrFail($id);
        //Obtener catalogo empresa

        $bg = BG::where('id_empresa', $id)->doesntExist();
        $er = ER::where('id_empresa', $id)->doesntExist();
        $mensaje = collect(['mensaje' => 'Seleccione las cuentas']);

        if ($bg || $er) {
            $mensaje = collect(['mensaje' => 'Debe agregar el catálogo']);
        };

        //Grupo Ratio
        $grupoRatio = RatioGrupo::collection(ModelsRatioGrupo::all());
        $collection = collect([]);

        foreach ($grupoRatio as $l) {
            //Objeto cantidad actual de ratio por grupo
            $cantidad = collect(['cantidad' =>
            RatioDetalle::join('ratio', 'ratiodetalle.id_ratio_base', '=', 'ratio.id_ratio_base')
                ->where('id_empresa', $id)
                ->where('ratio.id_ratio_grupo', $l->id_ratio_grupo)
                ->count()]);

            //Lista de ratios por grupo
            $busqueda = collect(['lista' =>
            collect(ModelsRatio::where('id_ratio_grupo', $l->id_ratio_grupo)
                ->get())]);

            $l = collect($l)
                ->merge($busqueda)
                ->merge($cantidad);

            $collection->push($l);
        }
        $bg = BalanceGeneralBase::where('id_empresa', $id)->get();
        $er = EstadoResultadoBase::where('id_empresa', $id)->get();

        return collect(['dato_ratio' => $collection, 'cuentas' => collect($bg)->merge(collect($er))])->merge($mensaje);
    }

    public function obtenerMensaje(Request $request)
    {
        $sector = (object)$request->sector;
        $sectorAnio = $sector->anio;
        $sectorDesc = $sector->descripcion;

        $anio = (object) $request->anio;
        $anio = $anio->anio;
        $idEmpresa = $request->empresa;

        $valoresObtenidos = $this->calculoRatios($idEmpresa, $anio);
        //dd($valoresObtenidos);
        $i = 0;

        $valoresComparados = RatioComparativo::where('descripcion', $sectorDesc)
            ->where('anio', $sectorAnio)
            ->orderBy('id_ratio_base', 'asc')
            ->get()
            ->pluck('valor');

        $resultado = collect([]);

        foreach ($valoresObtenidos as $current) {
            $idBuscar = $current['id_ratio_base'];
            $temporal = collect([
                'nombre' => $current['nombre_ratio'],
                'valorObtenido' => $current['valorObtenido'],
                'valorComparado' => $valoresComparados[$i],
            ]);
            $mensaje = RatioMensaje::where('id_ratio_base', $idBuscar)->get();
            if ($current['valorObtenido'] == $valoresComparados[$i]) {
                $temporal->put('mensaje', $mensaje->pluck('igual')[0]);
            }
            if ($current['valorObtenido'] > $valoresComparados[$i]) {
                $temporal->put('mensaje', $mensaje->pluck('mayor')[0]);
            }
            if ($current['valorObtenido'] < $valoresComparados[$i]) {
                $temporal->put('mensaje', $mensaje->pluck('menor')[0]);
            }
            $resultado->push($temporal);
            $i = $i + 1;
        }
        return response()->json(["dato" => $resultado]);
    }


    public function calculoRatios($idEmpresa, $anio)
    {
        $listaAnioBG = DB::table('balancegeneralbase')
            ->join('balancegeneral', 'balancegeneral.id_balance_base', '=', 'balancegeneralbase.id_balance_base')
            ->orderBy('anio', 'asc')
            ->select('anio')
            ->where('id_empresa', $idEmpresa)
            ->distinct()
            ->pluck('anio');

        $listaAnioER = DB::table('estadoresultadobase')
            ->join('estadoresultado', 'estadoresultado.id_estado_base', '=', 'estadoresultadobase.id_estado_base')
            ->orderBy('anio', 'asc')
            ->select('anio')
            ->where('id_empresa', $idEmpresa)
            ->distinct()
            ->pluck('anio');

        $ratioConfigurado = DB::table('ratiodetalle')->where('id_empresa', $idEmpresa)
            ->count();

        $ratio =  DB::table('ratio')
            ->count();

        $filas = collect([]);

        foreach ($listaAnioBG as $l) {
            if ($listaAnioER->contains($l)) {
                $filas->push($l);
            }
        }
        if (!$filas->contains($anio)) {
            return "Este año no tiene BG o ER";
        }

        if (count($filas) == 0) {
            dump("No se pueden calcular");
            return;
        }

        if ($ratioConfigurado != $ratio) {
            return "Debe configurar los ratios";
        }

        $filtro = ModelsRatio::join('ratiodetalle', 'ratiodetalle.id_ratio_base', '=', 'ratio.id_ratio_base')
            ->join('ratiogrupo', 'ratiogrupo.id_ratio_grupo', '=', 'ratio.id_ratio_grupo')
            ->select('ratio.nombre AS nombre_ratio', 'ratiogrupo.nombre AS nombre_grupo', 'operacion', 'campo1', 'campo2', 'campo3', 'ratio.id_ratio_base')
            ->where('ratiodetalle.id_empresa', $idEmpresa)
            ->get();

        $resultadoRetornar = collect([]);
        foreach ($filtro as $f) {
            if ($f->operacion == 1) {
                $a = $this->obtenerValor($f->campo1, $anio, $idEmpresa);
                $b = $this->obtenerValor($f->campo2, $anio, $idEmpresa);
                $r = $this->division($a, $b);
            }
            if ($f->operacion == 2) {
                $a = $this->obtenerValor($f->campo1, $anio, $idEmpresa);
                $b = $this->obtenerValor($f->campo2, $anio, $idEmpresa);
                $r = $this->multDiv($a, $b);
            }
            if ($f->operacion == 3) {
                $a = $this->obtenerValor($f->campo1, $anio, $idEmpresa);
                $b = $this->obtenerValor($f->campo2, $anio, $idEmpresa);
                $c = $this->obtenerValor($f->campo3, $anio, $idEmpresa);
                $r = $this->restaDiv($a, $b, $c);
            }
            if ($f->operacion == 4) {
                $a = $this->obtenerValor($f->campo1, $anio, $idEmpresa);
                $b = $this->obtenerValor($f->campo2, $anio, $idEmpresa);
                $c = $this->obtenerValor($f->campo3, $anio, $idEmpresa);
                $r = $this->sumaDiv($a, $b, $c);
            }
            if ($f->operacion == 5) {
                $a = $this->obtenerValor($f->campo1, $anio, $idEmpresa);
                $b = $this->obtenerValor($f->campo2, $anio, $idEmpresa);
                $r = $this->resta($a, $b);
            }
            if ($f->operacion == 6) {
                $a = $this->obtenerValor($f->campo1, $anio, $idEmpresa);
                $b = $this->obtenerValorConPromedio($f->campo2, $anio, $idEmpresa);
                $r = $this->division($a, $b);
            }
            if ($f->operacion == 7) {
                $a = $this->obtenerValorConPromedio($f->campo1, $anio, $idEmpresa);
                $b = $this->obtenerValor($f->campo2, $anio, $idEmpresa);
                $r = $this->multDiv($a, $b);
            }

            $f = collect($f)->merge(collect(['valorObtenido' => round($r, 2)]));
            $resultadoRetornar->push($f);
        }
        return $resultadoRetornar;
    }

    //Obtiene el valor de una cuenta de acuerdo a la empresa, el año y el identificador que recibe.
    public function obtenerValor($identificador, $anio, $idEmpresa)
    {
        //Hacia Estado Resultado
        if (strlen($identificador) == 3) {
            $valor = EstadoResultadoBase::join('estadoresultado as er', 'estadoresultadobase.id_estado_base', '=', 'er.id_estado_base')
                ->select('estadoresultadobase.nombre_cuenta', 'er.valor')
                ->where('estadoresultadobase.identificador', $identificador)
                ->where('er.anio', $anio)
                ->where('estadoresultadobase.id_empresa', $idEmpresa)
                ->get();
            return $valor[0]->valor;
        }

        //Hacia Balance General
        $valor = BalanceGeneralBase::join('balancegeneral as bg', 'balancegeneralbase.id_balance_base', '=', 'bg.id_balance_base')
            ->select('balancegeneralbase.nombre_cuenta', 'bg.valor')
            ->where('balancegeneralbase.identificador', $identificador)
            ->where('bg.anio', $anio)
            ->where('balancegeneralbase.id_empresa', $idEmpresa)
            ->get();
        return $valor[0]->valor;
    }

    //Calculo de valores con PROMEDIO
    public function obtenerValorConPromedio($identificador, $anio, $idEmpresa)
    {
        //Hacia Estado Resultado
        if (strlen($identificador) == 3) {
            $valor = EstadoResultadoBase::join('estadoresultado as er', 'estadoresultadobase.id_estado_base', '=', 'er.id_estado_base')
                ->where('estadoresultadobase.identificador', $identificador)
                ->where('er.anio', '<=', $anio)
                ->where('estadoresultadobase.id_empresa', $idEmpresa)
                ->avg('er.valor');
            return $valor;
        }

        //Hacia Balance General
        $valor = BalanceGeneralBase::join('balancegeneral as bg', 'balancegeneralbase.id_balance_base', '=', 'bg.id_balance_base')
            ->where('balancegeneralbase.identificador', $identificador)
            ->where('bg.anio', '<=', $anio)
            ->where('balancegeneralbase.id_empresa', $idEmpresa)
            ->avg('bg.valor');
        return $valor;
    }

    //Operacion 1
    public function division($val1, $val2)
    {
        return $val2 == 0 ?  0 : $val1 / $val2;
    }
    //Operacion 2
    public function multDiv($val1, $val2)
    {
        return $val2 == 0 ?  0 : ($val1 * 365) / $val2;
    }
    //Operacion 3
    public function restaDiv($val1, $val2, $val3)
    {
        return $val3 == 0 ?  0 : ($val1 - $val2) / $val3;
    }
    //Operacion 4
    public function sumaDiv($val1, $val2, $val3)
    {
        return $val3 == 0 ?  0 : ($val1 + $val2) / $val3;
    }
    //operacion 5
    public function resta($val1, $val2)
    {
        return $val1 - $val2;
    }

    //***************CRUD de Ratios*******************

    //Metodo para mostrar la lista de Ratios
    public function index()
    {

        /* $ratios = new Ratio();
        return response()->json($ratios); */
        return view('ratio.ratioCRU');
    }

    public function listar()
    {
        $ratios = DB::table('ratio')->get();
        return response()->json($ratios);
    }

    public function guardarRatio(ratioRequest $request)
    {
        $ratio = new ModelsRatio;
        $ratio->id_ratio_grupo = request()->id_ratio_grupo;
        $ratio->nombre = request()->nombre;
        $ratio->posicion = request()->posicion;
        $ratio->operacion = request()->operacion;
        $ratio->save();
        return;
    }

    public function editarRatio($id)
    {
        $ratio =  DB::table('ratio')->where('id_ratio_base', $id)->first();
        return response()->json($ratio);
    }

    public function actualizarRatio(ratioRequest $request)
    {
        $ratio = ModelsRatio::find(request()->id_ratio_base);
        $ratio->id_ratio_grupo = request()->id_ratio_grupo;
        $ratio->nombre = request()->nombre;
        $ratio->posicion = request()->posicion;
        $ratio->operacion = request()->operacion;
        $ratio->update();
        return;
    }

    public function eliminarRatio($id)
    {
        $ratio = ModelsRatio::find($id)->delete();
        return response()->json($ratio);
    }
}
