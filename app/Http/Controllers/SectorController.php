<?php

namespace App\Http\Controllers;
use App\Models\Sector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SectorController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*----- ACCESO -----------*/
        // Auth::user()->acceso('031');
        /*----------------------*/
        $sector=Sector::paginate(5);
        return view('sector/inicio' ,  compact('sector'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sectorAgregar = new Sector;
        $request->validate([
            'nombre'=> 'required',
            'descripcion' => 'required',
        ]);
        $sectorAgregar->nombre=$request->nombre;
        $sectorAgregar->descripcion=$request->descripcion;
        $sectorAgregar->save();
        return back()->with('agregar', 'El sector ha sido agregado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_sector)
    {   
        $sectorActualizar=Sector::findOrFail($id_sector);
        return view('sector/editar' , compact('sectorActualizar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_sector)
    {
        $sectorUpdate = Sector::findOrFail($id_sector);
        $sectorUpdate->nombre = $request->nombre;
        $sectorUpdate->descripcion = $request->descripcion;
        $sectorUpdate->save();
        return back()-> with('update', 'El sector ha sido actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
