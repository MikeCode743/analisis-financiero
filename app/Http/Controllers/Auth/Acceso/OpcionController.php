<?php

namespace App\Http\Controllers\Auth\Acceso;

use App\Http\Controllers\Controller;
use App\Models\AccesoUsuario;
use App\Models\Empresa;
use App\Models\OpcionFormulario;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OpcionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function perfilOpciones($empr, $empl)
    {
        $empresa = Empresa::findOrFail($empr);
        $empleado = User::findOrFail($empl);
        $opcionesusuario = DB::table('accesousuario')
            ->join('opcionformulario', 'accesousuario.id_opcion', '=', 'opcionformulario.id_opcion')
            ->select('opcionformulario.id_opcion AS id_opcion', 'opcionformulario.descripcion_opcion AS descripcion')
            ->where('id_empresa', '=', $empresa->id_empresa)
            ->where('id_usuario', '=', $empleado->id)
            ->get();
        $opciones = DB::table('opcionformulario')
            ->select('id_opcion', 'descripcion_opcion AS descripcion')
            ->where('id_opcion', '!=', '000')
            ->where('id_opcion', '!=', '014')
            ->where('id_opcion', '!=', '015')
            ->where('id_opcion', '!=', '014')
            ->where('id_opcion', '!=', '016')
            ->where('id_opcion', '!=', '017')
            ->where('id_opcion', '!=', '019')
            ->where('id_opcion', '!=', '018')
            ->where('id_opcion', '!=', '024')
            ->get();
        foreach ($opcionesusuario as $opcU) {
            $opciones = $opciones->reject(function ($value) use ($opcU) {
                return $value->id_opcion == $opcU->id_opcion;
            });
        }
        return view('auth.acceso.perfilOpciones', [
            'empresa' => $empresa,
            'empleado' => $empleado,
            'opciones' => $opciones,
            'opcionesusuario' => $opcionesusuario
        ]);
    }

    public function destroy(Request $opcion, $empr,$empl){
        $empresa = Empresa::findOrFail($empr);
        $empleado = User::findOrFail($empl);
        $opc = DB::table('opcionformulario')
        ->where('id_opcion','=',$opcion->opcU)->first();

        if($empresa || $empleado || $opc){
            DB::table('accesousuario')
            ->where('id_empresa','=',$empresa->id_empresa)
            ->where('id_usuario','=',$empleado->id)
            ->where('id_opcion','=',$opc->id_opcion)
            ->delete();
        }
        return redirect()->back();
    }

    public function agregaracceso(Request $opcion, $empr,$empl){

        try {
        $empresa = Empresa::findOrFail($empr);
        $empleado = User::findOrFail($empl);
        $opc = DB::table('opcionformulario')
        ->where('id_opcion','=',$opcion->opc)->first();
        if($empresa || $empleado || $opc){
            AccesoUsuario::create([
                'id_empresa' => $empresa->id_empresa,
                'id_usuario' => $empleado->id,
                'id_opcion'=> $opc->id_opcion
            ]);
        }
        return redirect()->back();
        } catch (Exception $e) {
            return redirect()->back()->with('Mensage',$e->getMessage());
        }
    }


    
}
