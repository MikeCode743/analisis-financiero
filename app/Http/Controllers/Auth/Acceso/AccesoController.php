<?php

namespace App\Http\Controllers\Auth\Acceso;

use App\Http\Controllers\Controller;
use App\Models\AccesoUsuario;
use App\Models\OpcionFormulario;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AccesoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){


        $accesos = OpcionFormulario::all()->where('id_opcion','!=','000');
        $roles = array('administrador','empresario','empleado');

        return view('auth.acceso.opcion',[
            'accesos' => $accesos,
            'roles' => $roles
        ]);
    }


    public function store(Request $request){
        // dd($request->all());
        $nuevo = new OpcionFormulario;
        $crud = array('Ver','Crear','Editar','Eliminar');
        if($request->rol == 'administrador'){
            $ultimo = OpcionFormulario::where('id_opcion','LIKE','_1%')->select('id_opcion')->get()->last();
            ++$ultimo->id_opcion;
            for ($i=0; $i < 4; $i++) { 
                $nuevo = new OpcionFormulario;
                $nuevo->id_opcion = $i.$ultimo->id_opcion;
                $nuevo->descripcion_opcion = $request->descripcion.' ('.$crud[$i].')';
                $nuevo->numero_formulario = $ultimo->id_opcion;
                $nuevo->save();
            }

        }elseif($request->rol == 'empresario'){
            $ultimo = OpcionFormulario::where('id_opcion','LIKE','_2%')->select('id_opcion')->get()->last();
            ++$ultimo->id_opcion;
            for ($i=0; $i < 4; $i++) { 
                $nuevo = new OpcionFormulario;
                $nuevo->id_opcion = $i.$ultimo->id_opcion;
                $nuevo->descripcion_opcion = $request->descripcion.' ('.$crud[$i].')';
                $nuevo->numero_formulario = $ultimo->id_opcion;
                $nuevo->save();
            }
        }elseif ($request->rol == 'empleado') {
            $ultimo = OpcionFormulario::where('id_opcion','LIKE','_3%')->select('id_opcion')->get()->last();
            ++$ultimo->id_opcion;
            for ($i=0; $i < 4; $i++) { 
                $nuevo = new OpcionFormulario;
                $nuevo->id_opcion = $i.$ultimo->id_opcion;
                $nuevo->descripcion_opcion = $request->descripcion.' ('.$crud[$i].')';
                $nuevo->numero_formulario = $ultimo->id_opcion;
                $nuevo->save();
            }
        }else{
            return redirect()->back();
        }

        return redirect()->back();

    }

 

    public function update(Request $request, $id){
        
        if(OpcionFormulario::where('id_opcion','=',$id)){
            OpcionFormulario::where('id_opcion','=',$id)->update([
                'descripcion_opcion' => $request->editDescripcion
            ]);
        }
        return redirect()->back();

    }

    public function destroy($id){
        if(OpcionFormulario::where('id_opcion','=',$id)){
            OpcionFormulario::where('id_opcion','=',$id)->delete();
        }
        return redirect()->back();
    }

    public function validarempresario($nuevoempleado){
        return $validatedData = $nuevoempleado->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed']
        ]);
    }
}
