<?php

namespace App\Http\Controllers\Empresa;

use App\Http\Controllers\BalanceGeneralBase;
use App\Http\Controllers\Controller;
use App\Models\AccesoUsuario;
use App\Models\Empresa;
use App\Models\OpcionFormulario;
use App\Models\Sector;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;



class EmpresaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function inicio()
    {

        /*----- ACCESO -----------*/
        // Auth::user()->acceso('019');
        /*----------------------*/
        try {
            $empresas = DB::table('AccesoUsuario')
                ->join('empresa', 'accesousuario.id_empresa', '=', 'empresa.id_empresa')
                ->join('users', 'accesousuario.id_usuario', '=', 'users.id')
                ->join('sector', 'empresa.id_sector', '=', 'sector.id_sector')
                ->select('empresa.id_empresa', 'empresa.nombre AS titulo', 'empresa.descripcion AS descripcion', 'sector.nombre AS sector', 'users.name AS propietario')
                ->where('accesousuario.id_usuario', '=', Auth::user()->id)
                ->groupBy('id_empresa')
                ->get();
            $sectores = Sector::all();
            return view('Empresa.inicio', [
                'empresas' => $empresas,
                'sectores' => $sectores
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with('mensaje', $e->getMessage());
        }
    }

    public function store(Request $request)
    {
        $empresa = new Empresa();

        $empresa->nombre = $request->nombre;
        $empresa->descripcion = $request->descripcion;
        $empresa->id_sector = $request->sector;
        $empresa->save();

        $opciones = OpcionFormulario::where('id_opcion', '!=', '000')
            ->where('id_opcion','!=','014')
            ->where('id_opcion', '!=', '015')
            ->where('id_opcion', '!=', '014')
            ->where('id_opcion', '!=', '016')
            ->where('id_opcion', '!=', '017')
            ->where('id_opcion', '!=', '019')
        ->get();

        if ($opciones->isNotEmpty()) {

            foreach ($opciones as $opcion) {
                $acceso = new AccesoUsuario();
                $acceso->id_usuario = Auth::id();
                $acceso->id_opcion = $opcion->id_opcion;
                $acceso->id_empresa = $empresa->id_empresa;
                $acceso->save();
            }
        }



        return redirect()->route('empresa.detalle', ['id' => $empresa->id_empresa]);
    }

    public function show($id)
    {
        try {
            $empresa = Empresa::findOrFail($id);
            $empleados = DB::table('accesousuario')
                ->join('users', 'accesousuario.id_usuario', '=', 'users.id')
                ->select('users.id AS id', 'users.name AS nombre', 'users.email AS email')
                ->where('accesousuario.id_empresa', '=', $empresa->id_empresa)
                ->where('id_usuario', '!=', Auth::user()->id)
                ->groupBy('accesousuario.id_usuario')
                ->get();
            return view('empresa.detalle', [
                'empresa' => $empresa,
                'empleados' => $empleados,

            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with('mensaje', $e->getMessage());
        }
    }


    public function empleado(Request $request, $id)
    {
        try {
            $validatedData = $this->validarempleado($request);
            if (!$validatedData) {
                return redirect()->back();
            }
            $nuevoempleado = User::create([
                'name' => $validatedData['name'],
                'email' => $validatedData['email'],
                'password' => Hash::make($validatedData['password']),
            ]);
            $empresa = Empresa::findOrFail($id);
            $opciones = OpcionFormulario::where('id_opcion', '!=', '000')
                ->where('id_opcion', '!=', '014')
                ->where('id_opcion', '!=', '015')
                ->where('id_opcion', '!=', '014')
                ->where('id_opcion', '!=', '016')
                ->where('id_opcion', '!=', '017')
                ->where('id_opcion', '!=', '019')
                ->where('id_opcion', '!=', '024')

                // ->where('id_opcion', '!=', '018')
            ->get();
            return redirect()->route('empresa.empleado.detalle', [$empresa, $nuevoempleado]);
        } catch (\Exception $e) {
            return redirect()->back()->with('mensaje', $e->getMessage());
        }
    }

    public function eliminarEmpleado($empresa, $empleado)
    {
        try {
        $var = DB::table('accesousuario')
            ->where('id_empresa', $empresa)
            ->where('id_usuario', $empleado)
            ->get();
        if ($var->isNotEmpty()) {
            $var = DB::table('accesousuario')
                ->where('id_empresa', $empresa)
                ->where('id_usuario', $empleado)
                ->delete();
            User::findOrFail($empleado)->delete();
        }
        return redirect()->route('empresa.detalle', $empresa);
        } catch (\Exception $e) {
            return redirect()->back()->with('mensaje', $e->getMessage());
        }
    }

    public function validarempleado($nuevoempleado)
    {
        return $validatedData = $nuevoempleado->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed']
        ]);
    }
}
