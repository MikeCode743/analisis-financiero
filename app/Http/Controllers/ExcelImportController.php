<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Imports\ExcelImport;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class ExcelImportController extends Controller// implements WithStrictNullComparison
{
	//-------------------------PARA IMPORTAR DATOS SOBRE EL BALANCE GENERAL-----------------------------
    public function importarCatalogoExcel(Request $request)
    {
        $file = $request->file('catalogo');        
        $catalogo = array();
        $error = false;
        $mensaje = "Catálogo importado con éxito.";
        try{        	
            $excelArray = (new ExcelImport)->toArray($file)[0];
            //verifica catalogo vacio
            if(sizeof($excelArray) == 0){ $error = true; $mensaje = "Archivo vacío."; }
            for($i = 0; $i < sizeof($excelArray); $i++){
                //verifica que ambas celdas tengan valores
                if($excelArray[$i][0] == null || $excelArray[$i][1] == null){//c2|1--i1|0
                    $catalogo = array();
                    $error = true;
                    $mensaje = "El archivo tiene datos incompletos.";
                    break;                
                }
                //verifica que el nombre de la cuenta no sea numero
                if(is_numeric($excelArray[$i][1])){                    
                    $catalogo = array();
                    $error = true;
                    $mensaje = "El nombre de la cuenta no puede ser numero.";
                    break;
                }                
                array_push($catalogo, [$excelArray[$i][0], $excelArray[$i][1]]);
            }
        }catch(Exception $e){
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
            $catalogo = array();
            $error = true;
            $mensaje = "Ocurrio un error al importar.";            
        }
        return response()->json([
            'catalogoImportado' => $catalogo, 
            'error' => $error,
            'mensaje' => $mensaje,
        ]);
    }

    public function importarValoresExcel(Request $request)
    {
        $file = $request->file('valores');
        $valores = array();
        $error = false;
        $mensaje = "Valores importados con éxito.";
        try{
	        $excelArray = (new ExcelImport)->toArray($file)[0];        
	        for ($i = 0; $i < sizeof($excelArray); $i++) {                     
	            array_push($valores, [$excelArray[$i][0], $excelArray[$i][1], $excelArray[$i][2]]);
	        }
		}catch(Exception $e){
			echo 'Excepción capturada: '.$e->getMessage()."\n";
            $valores = array();
            $error = true;
            $mensaje = "Ocurrio un error al importar.";
		}
        return response()->json([
        	'valoresImportados' => $valores, 
        	'mensaje' => $mensaje,
        	'error' => $error
        ]);
    }  
}
