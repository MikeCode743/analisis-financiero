<?php

namespace App\Http\Controllers;

use App\Http\Resources\DatoEmpresa;

use App\Models\Empresa;
use App\Models\BalanceGeneral;
use App\Models\EstadoResultado;
use App\Models\Sector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EmpresaControlador extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function inicio()
    {

        /*----- ACCESO -----------*/
        // Auth::user()->acceso('019');
        /*----------------------*/

        $empresas = DB::table('AccesoUsuario')
        ->join('empresa', 'accesousuario.id_empresa', '=', 'empresa.id_empresa')
        ->join('users', 'accesousuario.id_usuario', '=', 'users.id')
        ->join('sector', 'empresa.id_sector', '=', 'sector.id_sector')
        ->select('empresa.id_empresa', 'empresa.nombre AS titulo', 'empresa.descripcion AS descripcion', 'sector.nombre AS sector', 'users.name AS propietario', 'sector.id_sector AS id_sector' )
        ->where('accesousuario.id_usuario', '=', Auth::user()->id)
            ->groupBy('id_empresa')
            ->get();
        $sectores = Sector::all();
        return view('Empresa.inicio', [
            'empresas' => $empresas,
            'sectores' => $sectores
        ]);

    }

    public function show($id)
    {
        /*----- ACCESO -----------*/
        Auth::user()->empresa($id);
        /*----------------------*/
        $empresa = Empresa::FindOrFail($id);
        return view('empresa.datoEmpresa', ['id' => $id, 'empresa' => $empresa]);
    }

    public function paneldeControl()
    {  
        $listadoEmpresa = Empresa::select('id_empresa', 'nombre')->get();
        $listadoSector = Sector::select('id_sector', 'nombre','descripcion')->get();        
        $dato = collect();
        $dato->push($listadoEmpresa);
        $dato->push($listadoSector);        
        return view('panelNavegacion', ['dato' => $dato]);
    }

    //API
    public function DatoEmpresa($filtro, $idempresa)
    {

        Empresa::FindOrFail($idempresa);
        if ($filtro == 0) {

            $listaAnio = DB::table('balancegeneralbase')
                ->join('balancegeneral', 'balancegeneral.id_balance_base', '=', 'balancegeneralbase.id_balance_base')
                ->orderBy('anio', 'asc')
                ->select('anio')
                ->where('id_empresa', $idempresa)
                ->distinct()
                ->get();
            $x = BalanceGeneral::orderBy('anio', 'asc')
                ->join('balancegeneralbase', 'balancegeneralbase.id_balance_base', '=', 'balancegeneral.id_balance_base')
                ->where('id_empresa', $idempresa)
                ->get();
            $x = collect(DatoEmpresa::collection($x)
                ->groupBy('identificador'));

            //Agrega el identificador 4000 para sumatoria de Pasivo y Patrimonio
            $pp = $x->only(['2000', '3000'])->flatten()->groupBy('anio');
            $totalpp = collect([]);
            foreach ($pp as $total) {
                $monto = $total->reduce(function ($carry, $item) {
                    return $carry + $item['valor'];
                });
                $agregar = [
                    'nomenclatura' => 'P&P',
                    'nombre' => 'Sumatoria Pasivo y Patrimonio',
                    'id' => '4000',
                    $total[0]['anio'] => $monto
                ];
                $totalpp->push($agregar);
            }
            $x = $x->put('4000',$totalpp);
            
            return response()->json(["dato" => $x, "listaAnio" => $listaAnio, "filtro" => "Balance General"]);
        }
        if ($filtro == 1) {

            $listaAnio = DB::table('estadoresultadobase')
                ->join('estadoresultado', 'estadoresultado.id_estado_base', '=', 'estadoresultadobase.id_estado_base')
                ->orderBy('anio', 'asc')
                ->select('anio')
                ->where('id_empresa', $idempresa)
                ->distinct()
                ->get();

            $x = EstadoResultado::orderBy('anio', 'asc')
                ->join('estadoresultadobase', 'estadoresultadobase.id_estado_base', '=', 'estadoresultado.id_estado_base')
                ->where('id_empresa', $idempresa)
                ->get();

            $x = collect(DatoEmpresa::collection($x)
                ->groupBy('identificador'));

            return response()->json(["dato" => $x, "listaAnio" => $listaAnio, "filtro" => "Estado de Resultado"]);
        }
        return response()->json(["mensaje" => 'No implementado'], 401);
    }
}
