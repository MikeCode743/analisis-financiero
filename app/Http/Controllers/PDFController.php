<?php

namespace App\Http\Controllers;

use App\Exports\CollectionExport;
use App\Exports\PDFExport;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use PHPUnit\Util\Json;

class PDFController extends Controller
{

    public function exportar(Request $request)
    {
        try {
            $datos = collect($request->datos)->toArray();
            $datos[0] = collect($datos[0])->replace([0 => 'Ratio', 1 => 'Comparación', 2 => 'Valor', 3 => 'Conclusion']);
            $nombreArchivo = 'archivo.pdf';
            $export = new PDFExport($datos);
            Excel::store($export, $nombreArchivo, 'public');
            Storage::disk('public')->path($nombreArchivo);
            return response()->json(['url' => asset(Storage::url($nombreArchivo))]);
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
            return response()->json(["error" => "Error al exportar."]);
        }
    }

    public function exportaranalisis(Request $request)
    {
        try {
            $datosExportar = $request->json()->all();
            $nombreArchivo = 'archivo.pdf';
            $export = new PDFExport($datosExportar);
            Excel::store($export, $nombreArchivo, 'public');
            Storage::disk('public')->path($nombreArchivo);
            return response()->json(['url' => asset(Storage::url($nombreArchivo))]);
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
            return response()->json(["error" => "Error al exportar."]);
        }
    }
    
}
