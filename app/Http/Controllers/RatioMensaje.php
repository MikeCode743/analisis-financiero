<?php

namespace App\Http\Controllers;

use App\Models\Empresa;
use App\Models\RatioComparativo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RatioMensaje extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function ratioMensaje($idEmpresa)
    {

        /*----- ACCESO -----------*/
        Auth::user()->opciones('014');
        /*----------------------*/

        $empresa = Empresa::FindOrFail($idEmpresa);
        $listaAnioBG = DB::table('balancegeneralbase')
            ->join('balancegeneral', 'balancegeneral.id_balance_base', '=', 'balancegeneralbase.id_balance_base')
            ->orderBy('anio', 'asc')
            ->select('anio')
            ->where('id_empresa', $idEmpresa)
            ->distinct()
            ->pluck('anio');
        $listaAnioER = DB::table('estadoresultadobase')
            ->join('estadoresultado', 'estadoresultado.id_estado_base', '=', 'estadoresultadobase.id_estado_base')
            ->orderBy('anio', 'asc')
            ->select('anio')
            ->where('id_empresa', $idEmpresa)
            ->distinct()
            ->pluck('anio');
        $listaAnio = collect([]);

        foreach ($listaAnioBG as $l) {
            if ($listaAnioER->contains($l)) {
                $listaAnio->push(['anio' => $l]);
            }
        }

        if (count($listaAnio) == 0) {
            return "No hay fechas en común";
        }

        $ratioConfigurado = DB::table('ratiodetalle')->where('id_empresa', $idEmpresa)
            ->count();
        $ratio =  DB::table('ratio')
            ->count();
        if ($ratioConfigurado != $ratio) {
            return "Debe configurar los ratios";
        }

        $listaRazon = RatioComparativo::where('id_sector', $empresa->id_sector)
            ->orderBy('anio', 'asc')
            ->get()->unique(function ($item) {
                return $item['anio'] . $item['descripcion'];
            });

        $tempRazon = collect([]);
        foreach ($listaRazon as $l) {
            $tempRazon->push(['anio' => $l['anio'], 'descripcion' => $l['descripcion']]);
        }

        if (count($tempRazon) == 0) {
            return "Debe agregar ratio comparativo";
        }

        return view('empresa.mensajeRazon', [
            'id' => $idEmpresa,
            'empresa' => $empresa,
            'listaAnio' => $listaAnio,
            'listaRazon' => $tempRazon
        ]);
    }
}
