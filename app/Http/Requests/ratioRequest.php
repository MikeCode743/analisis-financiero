<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ratioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'    =>'required|regex:/^[a-zA-Zíóúéáñ\s]+$/u',
            'id_ratio_grupo'=>'required',
            'operacion' => 'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El nombre es requerido',
            'nombre.regex' => 'El nombre no es valido',
            'id_ratio_grupo.required'=>'El grupo es requerido',
            'operacion.required' => 'La operacion es requerida',
            'operacion.numeric' => 'Ingrese un digito'
        ];
    }
}
