<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RatioGrupo extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id_ratio_grupo' => $this->id_ratio_grupo,
            'nombre' => $this->nombre,
            'catalogo' => json_decode($this->catalogo),
            'lista' => json_decode($this->lista),
        ];
    }
}
