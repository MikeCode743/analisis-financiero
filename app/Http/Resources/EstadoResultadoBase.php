<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EstadoResultadoBase extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id_estado_base' => $this->id_estado_base,
            'nomenclatura' => $this->nomenclatura,
            'nombre' => $this->nombre_cuenta,
            'id' => $this->identificador,
            'calculado' => $this->calculado,
            'decremento' => $this->decremento,
            'cuenta' => json_decode($this->calculado_configurado)
        ];
    }
}
