<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClasificacionRazon extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id_clasificacion_razon' => $this->id_clasificacion_razon,
            'nombre' => $this->nombre,
            'razon' => json_decode($this->razon),            
        ];
    }
}
