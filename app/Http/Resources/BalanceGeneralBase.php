<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BalanceGeneralBase extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id_balance_base' => $this->id_balance_base,
            'catalogo' => $this->nomenclatura,
            'nombre' => $this->nombre_cuenta,
            'id' => $this->identificador,
        ];
    }
}
