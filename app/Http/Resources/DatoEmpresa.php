<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;

class DatoEmpresa extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [                      
            'nomenclatura' => $this->nomenclatura,
            'nombre' => $this->nombre_cuenta,
            'id' => $this->identificador,
            $this->anio => $this->valor,            
        ];
    }
}
