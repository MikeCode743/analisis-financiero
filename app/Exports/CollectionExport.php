<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class CollectionExport implements FromArray, WithStyles, ShouldAutoSize, WithStrictNullComparison
{
    protected $filas;

    public function __construct(array $filas)
    {
        $this->filas = $filas;
    }

    public function array(): array
    {
        return $this->filas;
    }

    public function styles(Worksheet $sheet)
    {
        return [            
            1    => ['font' => ['bold' => true]],
        ];
    }
}
/*
class CollectionExport implements FromCollection, WithHeadings
{
    //use Exportable;    

    public function collection()
    {
        return collect([
            [
                'name' => 'Povilas',
                'surname' => 'Korop',
                'email' => 'povilas@laraveldaily.com',
                'twitter' => '@povilaskorop'
            ],
            [
                'name' => 'Taylor',
                'surname' => 'Otwell',
                'email' => 'taylor@laravel.com',
                'twitter' => '@taylorotwell'
            ]
        ]);
    }

    public function headings(): array
    {
        return [
            'Name',
            'Surname',
            'Email',
            'Twitter',
        ];
    }
}
*/