<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class PDFExport implements FromArray, WithStrictNullComparison
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct(array $filas)
    {
        $this->filas = $filas;
    }

    public function array(): array
    {
        return $this->filas;
    }
    // public function columnFormats(): array
    // {
    //     return [
            // 'C' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
    //     ];
    // }
}
