<?php

namespace App;

use App\Models\AccesoUsuario;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\PseudoTypes\False_;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //--------------------------Accesos-------------------------------------------

    public function tieneAcceso($acceso)
    {
        $tieneacceso = AccesoUsuario::where('id_opcion', '=', $acceso)->where('id_usuario', '=', Auth::id())->get();
        // dd(count($tieneacceso));
        if (count($tieneacceso) != 0) {
            return true;
        } else {
            return false;
        }
    }

    public function Acceso($acceso)
    {
        if (is_array($acceso)) {
            foreach ($acceso as $opcion) {
                if ($this->tieneAcceso($opcion)) {
                    return true;
                }
            }
        } else {
            if ($this->tieneAcceso($acceso)) {
                return true;
            }
        }
        return false;
    }

    public function empresa($empresa)
    {
        if ($this->Acceso('000')) {
            return true;
        }
        $esdelaempresa = AccesoUsuario::where('id_empresa', '=', $empresa)->where('id_usuario', '=', Auth::id())->get();
        if ($esdelaempresa->isEmpty()) {
            return abort(401, 'No estas autorizado');
        } else {
            return true;
        }
    }

    public function opciones($acceso)
    {
        if (is_array($acceso)) {
            foreach ($acceso as $opcion) {
                if (!$this->tieneAcceso($opcion)) {
                    return abort(401, 'No estas autorizado');
                }
            }
        } else {
            if (!$this->tieneAcceso($acceso)) {
                return abort(401, 'No estas autorizado');
            }
        }
        return false;
    }
}
