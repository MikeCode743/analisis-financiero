<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::view('/test', 'layouts.test');


//  *---- Gerardo ----*
//Sector 
Route::get('sector', 'SectorController@index')->name('sector.inicio');
Route::post('sector/agregar', 'SectorController@store')->name('sector.guardar');
Route::get('sector/editar/{id_sector}', 'SectorController@edit')->name('sector.editar');
Route::put('sector/update/{id_sector}', 'SectorController@update')->name('sector.update');

//RatioMensaje
Route::get('ratio/mensaje', 'RatiomensajeController@index')->name('mensaje.inicio');
Route::post('ratio/mensaje/guardar', 'RatiomensajeController@store')->name('mensaje.guardar');
Route::get('ratio/mensaje/edit/{id_mensaje}', 'RatiomensajeController@edit')->name('mensaje.edit');
Route::put('ratio/mensaje/update/{id_mensaje}', 'RatiomensajeController@update')->name('mensaje.update');

//RatioComparativo
Route::get('ratio/comparativo/detalle', 'RatiocompController@index')->name('comparativo.inicio');
//Route::post('ratio/comparativo/almacenar', 'RatiocompController@store')->name('storecomp');
Route::get('ratio/comparativo/edit/{id_comparativo}', 'RatiocompController@edit')->name('comparativo.edit');
Route::put('ratio/comparativo/update/{id_comparativo}', 'RatiocompController@update')->name('comparativo.update');
Route::get('ratio/comparativo/{id_sector}', 'RatiocompController@enviosector')->name('sector.comparativo');
Route::get('ratio/comparativo', 'RatiocompController@cargarRatio')->name('comparativo.guardar');
Route::post('ratio/comparativo/agregar', 'RatiocompController@agregarRatio');
Route::delete('ratio/comparativo/eliminar/{sector}/{anio}/{desc}', 'RatiocompController@destroy')->name('comparativo.eliminar');





//  *---- Eduardo ----*
//Quitar los controladores Repetido

//Importar de Excel
Route::post('importar-catalogo-balance', 'ExcelImportController@importarCatalogoExcel')->name('importar.balance.catalogo');
Route::post('importar-valores-balance', 'ExcelImportController@importarValoresExcel')->name('importar.balance.valores');
Route::post('importar-catalogo-resultados', 'ExcelImportController@importarCatalogoExcel')->name('importar.resultados.catalogo');
Route::post('importar-valores-resultados', 'ExcelImportController@importarValoresExcel')->name('importar.resultados.valores');

//Exportar a Excel
Route::post('/exportar-ratio-mensaje','ExcelExportController@exportar')->name('exportar.ratio.mensaje');
Route::post('/exportar-ratio-sector','ExcelExportController@exportar')->name('exportar.ratio.sector');
Route::post('/exportar-analisis','ExcelExportController@exportar')->name('exportar.analisis');

//Ratio Grupo
Route::get('ratio/grupo', 'RatioGrupoController@show')->name('ratio.grupo');
Route::post('ratio/grupo/crear', 'RatioGrupoController@store')->name('ratio.grupo.crear');
Route::post('ratio/grupo/modificar', 'RatioGrupoController@update')->name('ratio.grupo.actualizar');





//  *---- Carlos ----*
//Configuracion de BG y ER
Route::get('balancegeneral/{id}/configurar', 'BalanceGeneralBase@show')->name('balance.inicio')->where('id', '[0-9]+');
Route::get('balancegeneral/{id}/agregar', 'BalanceGeneralBase@show2')->name('balance.agregar')->where('id', '[0-9]+');

Route::get('estadoresultado/{id}/configurar', 'EstadoResultadoBase@show')->name('estado.inicio')->where('id', '[0-9]+');
Route::get('estadoresultado/{id}/agregar', 'EstadoResultadoBase@show2')->name('estado.agregar')->where('id', '[0-9]+');

//Configuracion de razones por empresa
Route::get('ratio/detalle/{id}/configurar', 'Ratio@configurarRatio')->where('id', '[0-9]+')->name('ratio.detalle.configurar');

//Valores de los ratios
Route::get('ratio/empresa/{id}', 'RatioMensaje@ratioMensaje')->where('id', '[0-9]+')->name('ratio.empresa');
Route::get('ratio/sector/{id}', 'ComparativaSector@empresaSector')->where('id', '[0-9]+')->name('ratio.sector');

//Tabla datos empresa
Route::get('empresa/{id}', 'EmpresaControlador@show')->where('id', '[0-9]+');
Route::get('navegacion', 'EmpresaControlador@paneldeControl')->name('balance.inicio');
Route::view('welcome', 'NavBar');

//Obtener tabla OCR
Route::view('captura/tabla', 'TableOCR.TableOCR')->name('captura.tabla');





//  *---- Miguel ----*
//-------------------BalanceGeneral----------------------------
Route::get('/estados/finacieros/empresa/{id}', 'BalanceGeneralBase@listabalances')->name('estadosfinancieros.lista');
Route::post('/estados/finacieros/delete', 'BalanceGeneralBase@eliminarbalance')->name('estadosfinancieros.eliminar');
Route::get('/conexion', 'ConexionDBController@conexion')->name('DB');
Route::post('/conexion', 'ConexionDBController@restaurar')->name('restaurar');

//Exportar PDF
Route::get('/exportar/pdf', 'PDFController@exportar')->name('exportar');
Route::get('/exportar/analisis/pdf', 'PDFController@exportar')->name('exportar');

//-------------------Empresa-------------------
Route::get('/empresa', 'EmpresaControlador@inicio')->name('empresa.inicio');
Route::post('/empresa', 'Empresa\EmpresaController@store')->name('empresa.store');
Route::get('/empresa/{id}/detalle', 'Empresa\EmpresaController@show')->name('empresa.detalle');
Route::post('/empresa/{id}/empleado', 'Empresa\EmpresaController@empleado')->name('empresa.empleado');
Route::delete('/empresa/{id}/empleado/{id_empleado}/eliminar', 'Empresa\EmpresaController@eliminarEmpleado')->name('empresa.empleado.destroy');

Route::get('/empresa/{id_empresa}/empleado/{id_empleado}', 'Auth\Acceso\OpcionController@perfilOpciones')->name('empresa.empleado.detalle');
Route::post('/empresa/{id_empresa}/empleado/{id_empleado}', 'Auth\Acceso\OpcionController@agregaracceso')->name('empresa.empleado.opcion');
Route::delete('/empresa/{id_empresa}/empleado/{id_empleado}', 'Auth\Acceso\OpcionController@destroy')->name('empresa.empleado.opcion');

Route::post('registrar', 'Auth\Acceso\AccesoController@nuevoEmpresario')->name('registrar');

//-------------------Administracion ------------
Route::get('/administracion/acceso', 'Auth\Acceso\AccesoController@index')->name('acceso.inicio');
Route::post('/administracion/acceso', 'Auth\Acceso\AccesoController@store')->name('acceso.store');
Route::post('/administracion/acceso/{id}', 'Auth\Acceso\AccesoController@update')->name('acceso.update');
Route::delete('/administracion/acceso/{id}', 'Auth\Acceso\AccesoController@destroy')->name('acceso.destroy');





//  *---- Daniel ----*
//CRU Ratios
Route::get('ratios', 'Ratio@index')->name('ratio.index');
Route::get('listar', 'Ratio@listar')->name('ratio.listar');
Route::post('guardarRatio', 'Ratio@guardarRatio')->name('ratio.guardar');
Route::get('editarRatio/{id}', 'Ratio@editarRatio')->name('ratio.editar');
Route::put('actualizarRatio', 'Ratio@actualizarRatio')->name('ratio.actualizar');
Route::delete('eliminarRatio/{id}', 'Ratio@eliminarRatio')->name('ratio.eliminar');