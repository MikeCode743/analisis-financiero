<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//  *---- Carlos ----*
//Guardar BASE configurar
Route::post('balancegeneral/create', 'BalanceGeneralBase@store');
Route::post('estadoresultado/create', 'EstadoResultadoBase@store');

//Estado Resultado agregar
Route::get('estadoresultado/{id}', 'EstadoResultadoBase@mostrarBase')->where('id', '[0-9]+');
Route::post('estadoresultado', 'EstadoResultadoBase@guardarEstado');

//Balance General agregar
Route::get('balancegeneral/{id}', 'BalanceGeneralBase@mostrarBase')->where('id', '[0-9]+');
Route::post('balancegeneral', 'BalanceGeneralBase@guardarBalance');

//Razones Financieras
Route::get('gruporatio/{id}','Ratio@mostrarClasificacion')->where('id', '[0-9]+');
Route::post('gruporatio/configurar', 'Ratio@configurarCuenta');

//Datos de la empresa
Route::get('datoempresa/{filtro}/{idempresa}', 'EmpresaControlador@DatoEmpresa')->where('filtro', '[0-9]+')->where('idempresa', '[0-9]+');

//Razon por empresa
Route::post('razon/{id}','Ratio@obtenerMensaje')->where('id', '[0-9]+');

//Razon por empresa
Route::post('ratio/sector','ComparativaSector@filtroComparativo')->where('id', '[0-9]+');

Route::post('exportar/pdf', 'PDFController@exportar');
Route::post('exportar/analisis/pdf', 'PDFController@exportaranalisis');
