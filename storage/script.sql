CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `opcionformulario` (
  `id_opcion` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion_opcion` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `numero_formulario` int(11) NOT NULL,
  PRIMARY KEY (`id_opcion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `sector` (
  `id_sector` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_sector`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `ratiogrupo` (
  `id_ratio_grupo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text COLLATE utf8_unicode_ci NOT NULL,
  `catalogo` text COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id_ratio_grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `empresa` (
  `id_empresa` int(11) NOT NULL AUTO_INCREMENT,
  `id_sector` int(11) NOT NULL,
  `nombre` text COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_empresa`),
  KEY `relationship_3_fk` (`id_sector`),
  CONSTRAINT `fk_relationship_3` FOREIGN KEY (`id_sector`) REFERENCES `sector` (`id_sector`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `accesousuario` (
  `id_usuario` bigint(20) DEFAULT NULL,
  `id_opcion` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_empresa` int(11) DEFAULT NULL,
  KEY `relationship_12_fk` (`id_usuario`),
  KEY `relationship_18_fk` (`id_opcion`),
  KEY `relationship_19_fk` (`id_empresa`),
  CONSTRAINT `fk_relationship_12` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`),
  CONSTRAINT `fk_relationship_18` FOREIGN KEY (`id_opcion`) REFERENCES `opcionformulario` (`id_opcion`),
  CONSTRAINT `fk_relationship_19` FOREIGN KEY (`id_empresa`) REFERENCES `empresa` (`id_empresa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `balancegeneralbase` (
  `id_balance_base` int(11) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(11) DEFAULT NULL,
  `nomenclatura` text COLLATE utf8_unicode_ci NOT NULL,
  `nombre_cuenta` text COLLATE utf8_unicode_ci NOT NULL,
  `identificador` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_balance_base`),
  KEY `relationship_26_fk` (`id_empresa`),
  CONSTRAINT `fk_relationship_26` FOREIGN KEY (`id_empresa`) REFERENCES `empresa` (`id_empresa`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `balancegeneral` (
  `id_balance` int(11) NOT NULL AUTO_INCREMENT,
  `id_balance_base` int(11) NOT NULL,
  `anio` int(11) NOT NULL,
  `valor` float NOT NULL,
  PRIMARY KEY (`id_balance`),
  KEY `relationship_21_fk` (`id_balance_base`),
  CONSTRAINT `fk_relationship_21` FOREIGN KEY (`id_balance_base`) REFERENCES `balancegeneralbase` (`id_balance_base`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `estadoresultadobase` (
  `id_estado_base` int(11) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(11) DEFAULT NULL,
  `nomenclatura` text COLLATE utf8_unicode_ci NOT NULL,
  `nombre_cuenta` text COLLATE utf8_unicode_ci NOT NULL,
  `identificador` text COLLATE utf8_unicode_ci NOT NULL,
  `calculado` int(11) DEFAULT NULL,
  `decremento` int(11) DEFAULT NULL,
  `calculado_configurado` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id_estado_base`),
  KEY `relationship_25_fk` (`id_empresa`),
  CONSTRAINT `fk_relationship_25` FOREIGN KEY (`id_empresa`) REFERENCES `empresa` (`id_empresa`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `estadoresultado` (
  `id_estado` int(11) NOT NULL AUTO_INCREMENT,
  `id_estado_base` int(11) NOT NULL,
  `anio` int(11) NOT NULL,
  `valor` float NOT NULL,
  PRIMARY KEY (`id_estado`),
  KEY `relationship_22_fk` (`id_estado_base`),
  CONSTRAINT `fk_relationship_22` FOREIGN KEY (`id_estado_base`) REFERENCES `estadoresultadobase` (`id_estado_base`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) NOT NULL,
  `connection` text COLLATE utf8_unicode_ci,
  `queue` text COLLATE utf8_unicode_ci,
  `payload` text COLLATE utf8_unicode_ci,
  `exception` text COLLATE utf8_unicode_ci,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(11) NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



CREATE TABLE IF NOT EXISTS `ratio` (
  `id_ratio_base` int(11) NOT NULL AUTO_INCREMENT,
  `id_ratio_grupo` int(11) NOT NULL,
  `nombre` text COLLATE utf8_unicode_ci NOT NULL,
  `posicion` text COLLATE utf8_unicode_ci NOT NULL,
  `operacion` int(11) NOT NULL,
  PRIMARY KEY (`id_ratio_base`),
  KEY `relationship_10_fk` (`id_ratio_grupo`),
  CONSTRAINT `fk_relationship_10` FOREIGN KEY (`id_ratio_grupo`) REFERENCES `ratiogrupo` (`id_ratio_grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `ratiocomparativo` (
  `id_comparativo` int(11) NOT NULL AUTO_INCREMENT,
  `id_sector` int(11) NOT NULL,
  `id_ratio_base` int(11) NOT NULL,
  `valor` float NOT NULL,
  `anio` int(11) NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_comparativo`),
  KEY `relationship_17_fk` (`id_sector`),
  KEY `relationship_15_fk` (`id_ratio_base`),
  CONSTRAINT `fk_relationship_15` FOREIGN KEY (`id_ratio_base`) REFERENCES `ratio` (`id_ratio_base`),
  CONSTRAINT `fk_relationship_17` FOREIGN KEY (`id_sector`) REFERENCES `sector` (`id_sector`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `ratiodetalle` (
  `id_ratio_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `id_ratio_base` int(11) NOT NULL,
  `id_empresa` int(11) NOT NULL,
  `campo1` text COLLATE utf8_unicode_ci NOT NULL,
  `campo2` text COLLATE utf8_unicode_ci NOT NULL,
  `campo3` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id_ratio_detalle`),
  KEY `relationship_11_fk` (`id_ratio_base`),
  KEY `relationship_13_fk` (`id_empresa`),
  CONSTRAINT `fk_relationship_11` FOREIGN KEY (`id_ratio_base`) REFERENCES `ratio` (`id_ratio_base`),
  CONSTRAINT `fk_relationship_13` FOREIGN KEY (`id_empresa`) REFERENCES `empresa` (`id_empresa`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `ratiomensaje` (
  `id_mensaje` int(11) NOT NULL AUTO_INCREMENT,
  `id_ratio_base` int(11) DEFAULT NULL,
  `mayor` text COLLATE utf8_unicode_ci NOT NULL,
  `menor` text COLLATE utf8_unicode_ci NOT NULL,
  `igual` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_mensaje`),
  KEY `relationship_14_fk` (`id_ratio_base`),
  CONSTRAINT `fk_relationship_14` FOREIGN KEY (`id_ratio_base`) REFERENCES `ratio` (`id_ratio_base`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;






