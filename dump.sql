-- MySQL dump 10.13  Distrib 5.7.32, for Linux (x86_64)
--
-- Host: localhost    Database: ProyectoANF
-- ------------------------------------------------------
-- Server version	5.7.32-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `ProyectoANF`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `ProyectoANF` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `ProyectoANF`;

--
-- Table structure for table `accesousuario`
--

DROP TABLE IF EXISTS `accesousuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accesousuario` (
  `id` bigint(20) DEFAULT NULL,
  `id_opcion` char(3) DEFAULT NULL,
  `id_empresa` int(11) DEFAULT NULL,
  KEY `relationship_12_fk` (`id`),
  KEY `relationship_18_fk` (`id_opcion`),
  KEY `relationship_19_fk` (`id_empresa`),
  CONSTRAINT `fk_relationship_12` FOREIGN KEY (`id`) REFERENCES `users` (`id`),
  CONSTRAINT `fk_relationship_18` FOREIGN KEY (`id_opcion`) REFERENCES `opcionformulario` (`id_opcion`),
  CONSTRAINT `fk_relationship_19` FOREIGN KEY (`id_empresa`) REFERENCES `empresa` (`id_empresa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accesousuario`
--

LOCK TABLES `accesousuario` WRITE;
/*!40000 ALTER TABLE `accesousuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `accesousuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `balancegeneral`
--

DROP TABLE IF EXISTS `balancegeneral`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `balancegeneral` (
  `id_balance` int(11) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(11) DEFAULT NULL,
  `anio` int(4) NOT NULL,
  `identificador` text NOT NULL,
  `valor` float NOT NULL,
  PRIMARY KEY (`id_balance`),
  KEY `relationship_21_fk` (`id_empresa`),
  CONSTRAINT `fk_relationship_21` FOREIGN KEY (`id_empresa`) REFERENCES `empresa` (`id_empresa`)
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `balancegeneral`
--

LOCK TABLES `balancegeneral` WRITE;
/*!40000 ALTER TABLE `balancegeneral` DISABLE KEYS */;
INSERT INTO `balancegeneral` VALUES (106,2,2020,'1000',1100000),(107,2,2020,'1100',560000),(108,2,2020,'1101',70000),(109,2,2020,'1102',40000),(110,2,2020,'1103',250000),(111,2,2020,'1104',200000),(112,2,2020,'1006',100000),(113,2,2020,'1007',440000),(114,2,2020,'2000',480000),(115,2,2020,'2100',280000),(116,2,2020,'2101',130000),(117,2,2020,'2102',120000),(118,2,2020,'2103',30000),(119,2,2020,'2200',200000),(120,2,2020,'2201',200000),(121,2,2020,'3000',620000),(122,2,2020,'3100',620000),(123,2,2020,'3101',150000),(124,2,2020,'3102',50000),(125,2,2020,'3103',200000),(126,2,2020,'3104',220000);
/*!40000 ALTER TABLE `balancegeneral` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `balancegeneralbase`
--

DROP TABLE IF EXISTS `balancegeneralbase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `balancegeneralbase` (
  `id_balance_base` int(11) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(11) DEFAULT NULL,
  `nomenclatura` text NOT NULL,
  `nombre_cuenta` text NOT NULL,
  `identificador` text NOT NULL,
  PRIMARY KEY (`id_balance_base`),
  KEY `relationship_11_fk` (`id_empresa`),
  CONSTRAINT `fk_relationship_11` FOREIGN KEY (`id_empresa`) REFERENCES `empresa` (`id_empresa`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `balancegeneralbase`
--

LOCK TABLES `balancegeneralbase` WRITE;
/*!40000 ALTER TABLE `balancegeneralbase` DISABLE KEYS */;
INSERT INTO `balancegeneralbase` VALUES (17,2,'A','Activos','1000'),(18,2,'AA','Activos Circulantes','1100'),(19,2,'AA1','Efectivo','1101'),(20,2,'AA2','Valores negociables','1102'),(21,2,'AA3','Cuentas por cobrar (neto)','1103'),(22,2,'AA4','Inventario','1104'),(23,2,'AB','Inversiones','1006'),(24,2,'AC','Planta y equipo neto','1007'),(25,2,'B','Pasivos','2000'),(26,2,'BA','Pasivos circulantes','2100'),(27,2,'BAA','Cuentas por pagar','2101'),(28,2,'BAB','Documentos por pagar','2102'),(29,2,'BAC','Impuestos devengados','2103'),(30,2,'BB','Pasivos a largo plazo','2200'),(31,2,'BBA','Bonos por pagar','2201'),(32,2,'C','Capital contable','3000'),(33,2,'CA','Acciones','3100'),(34,2,'CAA','Acciones preferentes, valor a la par, $100','3101'),(35,2,'CAB','Acciones comunes, valor a la par, $5','3102'),(36,2,'CAC','Capital pagado en exceso del valor a la par','3103'),(37,2,'CAD','Utilidades retenidas','3104');
/*!40000 ALTER TABLE `balancegeneralbase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa` (
  `id_empresa` int(11) NOT NULL AUTO_INCREMENT,
  `id_sector` int(11) NOT NULL,
  `nombre` text NOT NULL,
  `descripcion` text,
  PRIMARY KEY (`id_empresa`),
  KEY `relationship_3_fk` (`id_sector`),
  CONSTRAINT `fk_relationship_3` FOREIGN KEY (`id_sector`) REFERENCES `sector` (`id_sector`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa`
--

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
INSERT INTO `empresa` VALUES (1,1,'Producto Familia S.A','nuevo'),(2,1,'GILLIAM CORPORATION',NULL);
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estadoresultado`
--

DROP TABLE IF EXISTS `estadoresultado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estadoresultado` (
  `id_estado` int(11) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(11) DEFAULT NULL,
  `anio` int(4) NOT NULL,
  `identificador` text NOT NULL,
  `valor` float NOT NULL,
  PRIMARY KEY (`id_estado`),
  KEY `relationship_22_fk` (`id_empresa`),
  CONSTRAINT `fk_relationship_22` FOREIGN KEY (`id_empresa`) REFERENCES `empresa` (`id_empresa`)
) ENGINE=InnoDB AUTO_INCREMENT=160 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estadoresultado`
--

LOCK TABLES `estadoresultado` WRITE;
/*!40000 ALTER TABLE `estadoresultado` DISABLE KEYS */;
INSERT INTO `estadoresultado` VALUES (151,2,2020,'000',2400000),(152,2,2020,'001',1600000),(153,2,2020,'002',800000),(154,2,2020,'003',560000),(155,2,2020,'004',240000),(156,2,2020,'005',30000),(157,2,2020,'006',210000),(158,2,2020,'007',75000),(159,2,2020,'008',135000);
/*!40000 ALTER TABLE `estadoresultado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estadoresultadobase`
--

DROP TABLE IF EXISTS `estadoresultadobase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estadoresultadobase` (
  `id_estado_base` int(11) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(11) NOT NULL,
  `nomenclatura` text,
  `nombre_cuenta` text NOT NULL,
  `identificador` text NOT NULL,
  `calculado` tinyint(1) NOT NULL,
  `decremento` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_estado_base`),
  KEY `relationship_23_fk` (`id_empresa`),
  CONSTRAINT `fk_relationship_23` FOREIGN KEY (`id_empresa`) REFERENCES `empresa` (`id_empresa`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estadoresultadobase`
--

LOCK TABLES `estadoresultadobase` WRITE;
/*!40000 ALTER TABLE `estadoresultadobase` DISABLE KEYS */;
INSERT INTO `estadoresultadobase` VALUES (19,2,'E1','Ventas (a crédito))','000',0,0),(20,2,'E2','Menos: Costo de ventas','001',0,1),(21,2,'E3','Utilidad bruta','002',1,0),(22,2,'E4','Menos: Gastos de ventas y administrativos','003',0,1),(23,2,'E5','Utilidad de operación (EBIT)','004',1,0),(24,2,'E6','Menos: Gastos por intereses','005',0,1),(25,2,'E7','Utilidades antes de impuestos (EBT)','006',1,0),(26,2,'E8','Menos: Impuestos','007',0,1),(27,2,'E9','Utilidades después de impuestos (EAT)','008',1,0);
/*!40000 ALTER TABLE `estadoresultadobase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) NOT NULL,
  `connection` text,
  `queue` text,
  `payload` text,
  `exception` text,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(11) NOT NULL,
  `migration` varchar(255) DEFAULT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opcionformulario`
--

DROP TABLE IF EXISTS `opcionformulario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opcionformulario` (
  `id_opcion` char(3) NOT NULL,
  `descripcion_opcion` varchar(50) NOT NULL,
  `numero_formulario` int(11) NOT NULL,
  PRIMARY KEY (`id_opcion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opcionformulario`
--

LOCK TABLES `opcionformulario` WRITE;
/*!40000 ALTER TABLE `opcionformulario` DISABLE KEYS */;
/*!40000 ALTER TABLE `opcionformulario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ratio`
--

DROP TABLE IF EXISTS `ratio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ratio` (
  `id_ratio_base` int(11) NOT NULL AUTO_INCREMENT,
  `id_grupo` int(11) NOT NULL,
  `nombre` text NOT NULL,
  PRIMARY KEY (`id_ratio_base`),
  KEY `relationship_14_fk` (`id_grupo`),
  CONSTRAINT `fk_relationship_14` FOREIGN KEY (`id_grupo`) REFERENCES `ratiogrupo` (`id_grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ratio`
--

LOCK TABLES `ratio` WRITE;
/*!40000 ALTER TABLE `ratio` DISABLE KEYS */;
/*!40000 ALTER TABLE `ratio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ratiocomparativo`
--

DROP TABLE IF EXISTS `ratiocomparativo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ratiocomparativo` (
  `id_comparativo` int(11) NOT NULL AUTO_INCREMENT,
  `id_sector` int(11) DEFAULT NULL,
  `id_ratio_base` int(11) DEFAULT NULL,
  `valor` float NOT NULL,
  `anio` date NOT NULL,
  PRIMARY KEY (`id_comparativo`),
  KEY `relationship_17_fk` (`id_sector`),
  KEY `relationship_15_fk` (`id_ratio_base`),
  CONSTRAINT `fk_relationship_15` FOREIGN KEY (`id_ratio_base`) REFERENCES `ratio` (`id_ratio_base`),
  CONSTRAINT `fk_relationship_17` FOREIGN KEY (`id_sector`) REFERENCES `sector` (`id_sector`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ratiocomparativo`
--

LOCK TABLES `ratiocomparativo` WRITE;
/*!40000 ALTER TABLE `ratiocomparativo` DISABLE KEYS */;
/*!40000 ALTER TABLE `ratiocomparativo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ratiodetalle`
--

DROP TABLE IF EXISTS `ratiodetalle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ratiodetalle` (
  `id_ratio_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `id_ratio_base` int(11) DEFAULT NULL,
  `id_empresa` int(11) DEFAULT NULL,
  `campo1` text NOT NULL,
  `campo2` text NOT NULL,
  `campo3` text,
  PRIMARY KEY (`id_ratio_detalle`),
  KEY `relationship_16_fk` (`id_ratio_base`),
  KEY `relationship_20_fk` (`id_empresa`),
  CONSTRAINT `fk_relationship_16` FOREIGN KEY (`id_ratio_base`) REFERENCES `ratio` (`id_ratio_base`),
  CONSTRAINT `fk_relationship_20` FOREIGN KEY (`id_empresa`) REFERENCES `empresa` (`id_empresa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ratiodetalle`
--

LOCK TABLES `ratiodetalle` WRITE;
/*!40000 ALTER TABLE `ratiodetalle` DISABLE KEYS */;
/*!40000 ALTER TABLE `ratiodetalle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ratiogrupo`
--

DROP TABLE IF EXISTS `ratiogrupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ratiogrupo` (
  `id_grupo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text NOT NULL,
  `operaciones` json NOT NULL,
  `catalogo_necesario` json NOT NULL,
  PRIMARY KEY (`id_grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ratiogrupo`
--

LOCK TABLES `ratiogrupo` WRITE;
/*!40000 ALTER TABLE `ratiogrupo` DISABLE KEYS */;
INSERT INTO `ratiogrupo` VALUES (2,'Razones de Actividad','{\"ratio\": [{\"nombre\": \"Rot Inventario\", \"posicion\": [0, 1], \"operacion\": 1}, {\"nombre\": \"Rot CxC\", \"posicion\": [3, 4], \"operacion\": 2}, {\"nombre\": \"Rot CxP\", \"posicion\": [5, 3], \"operacion\": 2}]}','{\"catalogo\": [\"Ventas\", \"Compras\", \"Activos Totales\", \"Utilidad Bruta\", \"Patrimonio\"]}');
/*!40000 ALTER TABLE `ratiogrupo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ratiomensaje`
--

DROP TABLE IF EXISTS `ratiomensaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ratiomensaje` (
  `id_mensaje` int(11) NOT NULL AUTO_INCREMENT,
  `id_ratio_base` int(11) DEFAULT NULL,
  `mayor` text NOT NULL,
  `menor` text NOT NULL,
  `igual` text NOT NULL,
  PRIMARY KEY (`id_mensaje`),
  KEY `relationship_13_fk` (`id_ratio_base`),
  CONSTRAINT `fk_relationship_13` FOREIGN KEY (`id_ratio_base`) REFERENCES `ratio` (`id_ratio_base`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ratiomensaje`
--

LOCK TABLES `ratiomensaje` WRITE;
/*!40000 ALTER TABLE `ratiomensaje` DISABLE KEYS */;
/*!40000 ALTER TABLE `ratiomensaje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sector`
--

DROP TABLE IF EXISTS `sector`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sector` (
  `id_sector` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text NOT NULL,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`id_sector`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sector`
--

LOCK TABLES `sector` WRITE;
/*!40000 ALTER TABLE `sector` DISABLE KEYS */;
INSERT INTO `sector` VALUES (1,'Servicios','');
/*!40000 ALTER TABLE `sector` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `email_verified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `password` varchar(255) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (0,'Mike','hp15004@ues.edu.sv','2020-10-26 21:07:12','$2y$10$qwDTWuhG3Rc8vNjUMTMYf.WhTG/Y9TGi2hZmd/brVIQYtxLLHUNDm',NULL,'2020-10-26 21:07:12','2020-10-26 21:07:12');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-28 19:28:32
